-- MySQL dump 10.13  Distrib 5.6.16, for Win64 (x86_64)
--
-- Host: localhost    Database: parc
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `parc`
--

/*!40000 DROP DATABASE IF EXISTS `parc`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `parc` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `parc`;

--
-- Table structure for table `accessoire`
--

DROP TABLE IF EXISTS `accessoire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accessoire` (
  `id_Acces` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Acces` varchar(50) DEFAULT NULL,
  `type_Acces` varchar(50) DEFAULT NULL,
  `date_Achat_Acces` date DEFAULT NULL,
  `prix_Unitaire` int(11) DEFAULT NULL,
  `garantie_Acces` varchar(50) DEFAULT NULL,
  `Commentaire` varchar(50) DEFAULT NULL,
  `id_Machine` int(11) NOT NULL,
  `id_Marque` int(11) NOT NULL,
  `id_Materiel` int(11) NOT NULL,
  `id_Fourniseur` int(11) NOT NULL,
  PRIMARY KEY (`id_Acces`),
  KEY `FK_Accessoire_id_Machine` (`id_Machine`),
  KEY `FK_Accessoire_id_Marque` (`id_Marque`),
  KEY `FK_Accessoire_id_Materiel` (`id_Materiel`),
  KEY `FK_Accessoire_id_Fourniseur` (`id_Fourniseur`),
  CONSTRAINT `FK_Accessoire_id_Fourniseur` FOREIGN KEY (`id_Fourniseur`) REFERENCES `fournisseur` (`id_Fournisseur`),
  CONSTRAINT `FK_Accessoire_id_Machine` FOREIGN KEY (`id_Machine`) REFERENCES `machine` (`id_Machine`),
  CONSTRAINT `FK_Accessoire_id_Marque` FOREIGN KEY (`id_Marque`) REFERENCES `marque` (`id_Marque`),
  CONSTRAINT `FK_Accessoire_id_Materiel` FOREIGN KEY (`id_Materiel`) REFERENCES `materiel` (`id_Materiel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accessoire`
--

LOCK TABLES `accessoire` WRITE;
/*!40000 ALTER TABLE `accessoire` DISABLE KEYS */;
/*!40000 ALTER TABLE `accessoire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avoir4`
--

DROP TABLE IF EXISTS `avoir4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avoir4` (
  `id_Depart` int(11) NOT NULL AUTO_INCREMENT,
  `id_Ste` int(11) NOT NULL,
  PRIMARY KEY (`id_Depart`,`id_Ste`),
  KEY `FK_Avoir4_id_Ste` (`id_Ste`),
  CONSTRAINT `FK_Avoir4_id_Depart` FOREIGN KEY (`id_Depart`) REFERENCES `departement` (`id_Depart`),
  CONSTRAINT `FK_Avoir4_id_Ste` FOREIGN KEY (`id_Ste`) REFERENCES `societe` (`id_Ste`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avoir4`
--

LOCK TABLES `avoir4` WRITE;
/*!40000 ALTER TABLE `avoir4` DISABLE KEYS */;
/*!40000 ALTER TABLE `avoir4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpu`
--

DROP TABLE IF EXISTS `cpu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cpu` (
  `id_Cpu` int(11) NOT NULL AUTO_INCREMENT,
  `cpu` text NOT NULL,
  PRIMARY KEY (`id_Cpu`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpu`
--

LOCK TABLES `cpu` WRITE;
/*!40000 ALTER TABLE `cpu` DISABLE KEYS */;
INSERT INTO `cpu` VALUES (2,'Dual Core ');
/*!40000 ALTER TABLE `cpu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departement`
--

DROP TABLE IF EXISTS `departement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departement` (
  `id_Depart` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Depart` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`nom_Depart`),
  UNIQUE KEY `id_Depart` (`id_Depart`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departement`
--

LOCK TABLES `departement` WRITE;
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
INSERT INTO `departement` VALUES (1,'Informatique'),(3,'Comptabilité');
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disposer`
--

DROP TABLE IF EXISTS `disposer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disposer` (
  `id_Machine` int(11) NOT NULL AUTO_INCREMENT,
  `id_Log` int(11) NOT NULL,
  PRIMARY KEY (`id_Machine`,`id_Log`),
  KEY `FK_Disposer_id_Log` (`id_Log`),
  CONSTRAINT `FK_Disposer_id_Log` FOREIGN KEY (`id_Log`) REFERENCES `logiciel` (`id_Log`),
  CONSTRAINT `FK_Disposer_id_Machine` FOREIGN KEY (`id_Machine`) REFERENCES `machine` (`id_Machine`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disposer`
--

LOCK TABLES `disposer` WRITE;
/*!40000 ALTER TABLE `disposer` DISABLE KEYS */;
/*!40000 ALTER TABLE `disposer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ecran`
--

DROP TABLE IF EXISTS `ecran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ecran` (
  `id_Ecran` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Ecran` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Ecran`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ecran`
--

LOCK TABLES `ecran` WRITE;
/*!40000 ALTER TABLE `ecran` DISABLE KEYS */;
INSERT INTO `ecran` VALUES (1,'Prolink 21\"');
/*!40000 ALTER TABLE `ecran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fournisseur` (
  `id_Fournisseur` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Fournisseur` varchar(50) NOT NULL DEFAULT '',
  `adresse_Fournisseur` varchar(50) DEFAULT NULL,
  `tel_Fournisseur` varchar(50) DEFAULT NULL,
  `email_Fournisseur` varchar(50) DEFAULT NULL,
  `site_Fournisseur` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nom_Fournisseur`),
  UNIQUE KEY `id_Fourniseur` (`id_Fournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fournisseur`
--

LOCK TABLES `fournisseur` WRITE;
/*!40000 ALTER TABLE `fournisseur` DISABLE KEYS */;
INSERT INTO `fournisseur` VALUES (1,'Concept','Citic et Suprem','12345','kjnkjsgnodls','lkdnflks'),(2,'Free Zone','','','','');
/*!40000 ALTER TABLE `fournisseur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hdd`
--

DROP TABLE IF EXISTS `hdd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hdd` (
  `id_Hdd` int(11) NOT NULL AUTO_INCREMENT,
  `hdd` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Hdd`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hdd`
--

LOCK TABLES `hdd` WRITE;
/*!40000 ALTER TABLE `hdd` DISABLE KEYS */;
INSERT INTO `hdd` VALUES (2,'Sata 250 GO');
/*!40000 ALTER TABLE `hdd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `intervention`
--

DROP TABLE IF EXISTS `intervention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `intervention` (
  `id_Interv` int(11) NOT NULL AUTO_INCREMENT,
  `date_Debut_Interv` date NOT NULL,
  `heur_Debut_Interv` time NOT NULL,
  `date_Fin_Interv` date NOT NULL,
  `heur_Fin_Interv` time NOT NULL,
  `nom_Rep` varchar(50) NOT NULL,
  `description_Interv` varchar(200) NOT NULL,
  `id_Machine` int(11) NOT NULL,
  PRIMARY KEY (`id_Interv`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `intervention`
--

LOCK TABLES `intervention` WRITE;
/*!40000 ALTER TABLE `intervention` DISABLE KEYS */;
INSERT INTO `intervention` VALUES (1,'2015-12-01','12:00:00','2015-12-02','12:00:00','NomRep','jkeabgkjabskjge',47),(2,'2015-12-01','12:12:12','2015-12-02','12:12:12','NomRep','jbsdkblgkjdsq',47),(3,'2015-12-10','12:00:00','2015-12-13','12:00:00','NomRep','jzntlqs',47),(4,'2012-10-13','10:00:00','2012-11-13','10:00:00','NomRep','FQDSYQD',46),(5,'2015-12-13','12:12:00','2015-12-14','13:13:00','NomRep','dsgdsgsd',50);
/*!40000 ALTER TABLE `intervention` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logiciel`
--

DROP TABLE IF EXISTS `logiciel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logiciel` (
  `id_Log` int(11) NOT NULL AUTO_INCREMENT,
  `type_Log` varchar(10) NOT NULL,
  `nom_log` varchar(50) DEFAULT NULL,
  `nom_Marque` varchar(50) NOT NULL,
  `date_Achat` date NOT NULL,
  `serial_log` varchar(50) DEFAULT NULL,
  `max_licence` varchar(50) NOT NULL,
  `prix_Achat` int(11) DEFAULT NULL,
  `nom_St` varchar(20) NOT NULL,
  `description_Log` varchar(100) NOT NULL,
  `id_Marque` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Log`),
  KEY `FK_Logiciel_id_Marque` (`id_Marque`),
  CONSTRAINT `FK_Logiciel_id_Marque` FOREIGN KEY (`id_Marque`) REFERENCES `marque` (`id_Marque`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logiciel`
--

LOCK TABLES `logiciel` WRITE;
/*!40000 ALTER TABLE `logiciel` DISABLE KEYS */;
INSERT INTO `logiciel` VALUES (1,'Gratuit','Adobe Photoshop CS5','HP','2015-12-06','1234564896','',12356,'Auto Diff','wfqdgqgqg',NULL),(2,'Payant','vlc 2.0','HP','2016-02-18','1d3z','',23013,'TOA','ezgezg',NULL),(15,'Gratuit','v','Asus','2016-02-18','132','31',5351,'Auto Diff','',NULL);
/*!40000 ALTER TABLE `logiciel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine`
--

DROP TABLE IF EXISTS `machine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine` (
  `id_Machine` int(11) NOT NULL AUTO_INCREMENT,
  `situation` varchar(10) NOT NULL,
  `type` varchar(15) NOT NULL,
  `nom_util` varchar(50) NOT NULL,
  `lieu` varchar(50) NOT NULL,
  `nom_St` varchar(20) NOT NULL,
  `nom_Machine` varchar(50) NOT NULL DEFAULT '',
  `adresse_IP` varchar(20) DEFAULT NULL,
  `date_Achat` date DEFAULT NULL,
  `duree_Garantie` varchar(10) DEFAULT NULL,
  `prix_Machine` varchar(30) DEFAULT NULL,
  `modele_Machine` varchar(50) DEFAULT NULL,
  `cpu` varchar(50) DEFAULT NULL,
  `ram` varchar(50) DEFAULT NULL,
  `ecran` varchar(50) DEFAULT NULL,
  `hdd` varchar(50) DEFAULT NULL,
  `autre_Description` varchar(70) DEFAULT NULL,
  `id_Marque` int(11) DEFAULT NULL,
  `id_Fournisseur` int(11) DEFAULT NULL,
  `nom_Marque` varchar(50) NOT NULL,
  `nom_Fournisseur` varchar(50) NOT NULL,
  `nom_Syst` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Machine`),
  UNIQUE KEY `id_Machine` (`id_Machine`),
  KEY `FK_Machine_id_Marque` (`id_Marque`),
  KEY `FK_Machine_id_Fourniseur` (`id_Fournisseur`),
  CONSTRAINT `FK_Machine_id_Fourniseur` FOREIGN KEY (`id_Fournisseur`) REFERENCES `fournisseur` (`id_Fournisseur`),
  CONSTRAINT `FK_Machine_id_Marque` FOREIGN KEY (`id_Marque`) REFERENCES `marque` (`id_Marque`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine`
--

LOCK TABLES `machine` WRITE;
/*!40000 ALTER TABLE `machine` DISABLE KEYS */;
INSERT INTO `machine` VALUES (46,'En service','Station','Riana','Villa Pradon Antanimena','TOA','tes','   .   .   .   ','2012-05-10','12','5000000','   i3','Dual Core ','2 Go DDR2','Prolink 21\"','Sata 250 GO','test',NULL,NULL,'HP','Concept','Windows 7 Integral 64 bits'),(47,'En stock','Station','','','Auto Diff','LMSF','121.222.124.210','2012-01-21','123','123',' L?MHSH','Dual Core ','2 Go DDR2','Prolink 21\"','Sata 250 GO','',NULL,NULL,'HP','Concept',''),(49,'En stock','Portable','','','SODIATRANS','blabla','134.856.242.312','2012-12-12','123','563132','  jkgnsdlg123','Dual Core ','2 Go DDR2','','Sata 250 GO','bjkbzdlkjsbg:s',NULL,NULL,'HP','Concept',''),(50,'En service','Portable','lova','','SODIATRANS','fh','152.345.632.135','2012-12-12','56312','456','   fdhdfh','hdfh','123','','46343','hsddfhd',NULL,NULL,'HP','Concept','Windows 8 Pro 64 bits'),(51,'En service','Station','Riana','Villa Pradon Antanimena','Cap Mada','f','123.156.123.165','2012-04-21','12','123523','  jvgbkjlm','Dual Core ','2 Go DDR2','Prolink 21\"','Sata 250 GO','',NULL,NULL,'HP','Concept',''),(52,'En stock','Station','lova','','','tes','192.168.056.250','2012-05-10','12','5000000','i3','4','1 Go','Prolink','500 Go','test',NULL,NULL,'HP','Concept',''),(53,'En stock','Station','','','TOA','Sitra','123.456.786.789','2007-06-16','12','1513213','  kfhkf','jdgj','jdjd','Prolink 21\"','jdrj','',NULL,NULL,'Asus','Concept',''),(54,'En stock','Station','','','TOA','Sitraka pc','123.456.786.789','2007-06-16','12','1513213',' kfhkf','jdgj','jdjd','Prolink 21\"','jdrj','',NULL,NULL,'Asus','Concept',''),(57,'En service','Portable','lova','','','fh','152.345.632.135','2012-12-12','56312','456',' fdhdfh','hdfh','123','','46343','hsddfhd',NULL,NULL,'HP','Concept',''),(58,'En stock','Station','Riana','','Auto Diff','jfkab','192.168.056.250','2002-04-13','123','12453','  jkgrbzl','zg','ehez','Prolink 21\"','hezhez','',NULL,NULL,'Asus','Concept',''),(59,'En stock','Station','lova','','Auto Diff','tes','192.168.056.250','2012-05-10','12','5000000','  i3','1','4','Prolink 21\"','1 Go','Prolink',NULL,NULL,'HP','Concept','Windows 7 Integral 64 bits'),(60,'En stock','Portable','','','','blabla','134.856.242.312','2012-12-12','123','563132','jkgnsdlg123','jkgksdg','123','','132','bjkbzdlkjsbg:s',NULL,NULL,'HP','Concept',''),(62,'En stock','Station','lova','','','tes','192.168.056.250','2012-05-10','12','5000000','i3','1','4','Prolink 21\"','1 Go','Prolink',NULL,NULL,'HP','Concept','Windows 7 Integral 64 bits'),(63,'En stock','Portable','lova','','','tes','192.168.056.250','2012-05-10','12','5000000','  i3','4','1 Go','Prolink 21\"','500 Go','test',NULL,NULL,'HP','Concept','Windows 7 Integral 64 bits'),(64,'En service','Station','Sitraka','Pradon','Auto Diff','test-pc','   .   .   .   ','2012-12-12','12','1231546','','Dual core','4 Go','Prolink 21\"','500 Go','',NULL,NULL,'HP','Free Zone','Windows 7 Integral 64 bits'),(66,'En service','Station','Riana','Villa Pradon Antanimena','Auto Diff','','   .   .   .   ','2016-02-27','12','12','','Dual Core ','2 Go DDR2','Prolink 21\"','Sata 250 GO','',NULL,NULL,'dell','Concept','Windows XP SP3'),(67,'En service','Station','Riana','Villa Pradon Antanimena','Auto Diff','jgf','123.154.352.123','2016-02-29','13','31','32','Dual Core ','2 Go DDR2','Prolink 21\"','Sata 250 GO','',NULL,NULL,'Asus','Concept','Windows 7 Integral 64 bits');
/*!40000 ALTER TABLE `machine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marque`
--

DROP TABLE IF EXISTS `marque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marque` (
  `id_Marque` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Marque` varchar(30) NOT NULL DEFAULT '',
  PRIMARY KEY (`nom_Marque`),
  UNIQUE KEY `id_Marque` (`id_Marque`),
  UNIQUE KEY `id_Marque_2` (`id_Marque`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marque`
--

LOCK TABLES `marque` WRITE;
/*!40000 ALTER TABLE `marque` DISABLE KEYS */;
INSERT INTO `marque` VALUES (1,'Asus'),(3,'dell'),(4,'HP'),(5,'Acer'),(6,'Canon');
/*!40000 ALTER TABLE `marque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materiel`
--

DROP TABLE IF EXISTS `materiel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materiel` (
  `id_Materiel` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Mat` varchar(50) DEFAULT NULL,
  `nom_Marque` varchar(15) NOT NULL,
  `nom_Fournisseur` varchar(20) NOT NULL,
  `date_Achat_Mat` date DEFAULT NULL,
  `duree_Garantie_Mat` int(11) DEFAULT NULL,
  `prix_Mat` int(11) DEFAULT NULL,
  `mat_En_Stock` varchar(10) DEFAULT NULL,
  `nom_St` varchar(20) NOT NULL,
  `description_Mat` varchar(50) DEFAULT NULL,
  `id_Marque` int(11) DEFAULT NULL,
  `id_Fourniseur` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Materiel`),
  KEY `FK_Materiel_id_Marque` (`id_Marque`),
  KEY `FK_Materiel_id_Fourniseur` (`id_Fourniseur`),
  CONSTRAINT `FK_Materiel_id_Fourniseur` FOREIGN KEY (`id_Fourniseur`) REFERENCES `fournisseur` (`id_Fournisseur`),
  CONSTRAINT `FK_Materiel_id_Marque` FOREIGN KEY (`id_Marque`) REFERENCES `marque` (`id_Marque`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materiel`
--

LOCK TABLES `materiel` WRITE;
/*!40000 ALTER TABLE `materiel` DISABLE KEYS */;
INSERT INTO `materiel` VALUES (2,'Imprimante','Asus','Concept','2016-01-12',45,450000,'En stock','Auto Diff','azezeaz',1,1),(3,'ugbjk','Acer','Concept','2016-02-22',4123,45123,'En service','Auto Diff','',NULL,NULL),(8,'gdg','dell','Concept','2016-02-24',21,12,'En service','Auto Diff','',NULL,NULL);
/*!40000 ALTER TABLE `materiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panne`
--

DROP TABLE IF EXISTS `panne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panne` (
  `id_Panne` int(11) NOT NULL AUTO_INCREMENT,
  `date_Panne` datetime DEFAULT NULL,
  `debut_Panne` datetime DEFAULT NULL,
  `fin_Panne` datetime DEFAULT NULL,
  `resultat_Panne` tinyint(1) DEFAULT NULL,
  `description_Panne` varchar(50) DEFAULT NULL,
  `id_Machine` int(11) NOT NULL,
  `id_Materiel` int(11) NOT NULL,
  `id_Acces` int(11) NOT NULL,
  PRIMARY KEY (`id_Panne`),
  KEY `FK_Panne_id_Machine` (`id_Machine`),
  KEY `FK_Panne_id_Materiel` (`id_Materiel`),
  KEY `FK_Panne_id_Acces` (`id_Acces`),
  CONSTRAINT `FK_Panne_id_Acces` FOREIGN KEY (`id_Acces`) REFERENCES `accessoire` (`id_Acces`),
  CONSTRAINT `FK_Panne_id_Machine` FOREIGN KEY (`id_Machine`) REFERENCES `machine` (`id_Machine`),
  CONSTRAINT `FK_Panne_id_Materiel` FOREIGN KEY (`id_Materiel`) REFERENCES `materiel` (`id_Materiel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panne`
--

LOCK TABLES `panne` WRITE;
/*!40000 ALTER TABLE `panne` DISABLE KEYS */;
/*!40000 ALTER TABLE `panne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ram`
--

DROP TABLE IF EXISTS `ram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ram` (
  `id_Ram` int(11) NOT NULL AUTO_INCREMENT,
  `ram` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Ram`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ram`
--

LOCK TABLES `ram` WRITE;
/*!40000 ALTER TABLE `ram` DISABLE KEYS */;
INSERT INTO `ram` VALUES (1,'2 Go DDR2');
/*!40000 ALTER TABLE `ram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reparateur`
--

DROP TABLE IF EXISTS `reparateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reparateur` (
  `matricul_Rep` varchar(20) NOT NULL,
  `nom_Rep` varchar(50) NOT NULL DEFAULT '',
  `prenom_Rep` varchar(50) DEFAULT NULL,
  `tel_Rep` varchar(20) DEFAULT NULL,
  `email_Rep` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`nom_Rep`),
  UNIQUE KEY `matricul_Rep` (`matricul_Rep`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reparateur`
--

LOCK TABLES `reparateur` WRITE;
/*!40000 ALTER TABLE `reparateur` DISABLE KEYS */;
INSERT INTO `reparateur` VALUES ('12345','NomRep','PreRep','156413213','kfhakj@bfjkas.bjke');
/*!40000 ALTER TABLE `reparateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reparer`
--

DROP TABLE IF EXISTS `reparer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reparer` (
  `id_Panne` int(11) NOT NULL AUTO_INCREMENT,
  `matricul_Rep` varchar(20) NOT NULL,
  PRIMARY KEY (`id_Panne`,`matricul_Rep`),
  KEY `FK_Reparer_matricul_Rep` (`matricul_Rep`),
  CONSTRAINT `FK_Reparer_id_Panne` FOREIGN KEY (`id_Panne`) REFERENCES `panne` (`id_Panne`),
  CONSTRAINT `FK_Reparer_matricul_Rep` FOREIGN KEY (`matricul_Rep`) REFERENCES `reparateur` (`matricul_Rep`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reparer`
--

LOCK TABLES `reparer` WRITE;
/*!40000 ALTER TABLE `reparer` DISABLE KEYS */;
/*!40000 ALTER TABLE `reparer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site` (
  `id_Site` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Site` varchar(50) NOT NULL,
  PRIMARY KEY (`id_Site`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site`
--

LOCK TABLES `site` WRITE;
/*!40000 ALTER TABLE `site` DISABLE KEYS */;
INSERT INTO `site` VALUES (2,'Villa Pradon Antanimena');
/*!40000 ALTER TABLE `site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `societe`
--

DROP TABLE IF EXISTS `societe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `societe` (
  `id_Ste` int(11) NOT NULL AUTO_INCREMENT,
  `nom_St` varchar(50) NOT NULL DEFAULT '',
  `adresse_Ste` varchar(50) DEFAULT NULL,
  `tel_Ste` varchar(20) DEFAULT NULL,
  `email_Ste` varchar(50) DEFAULT NULL,
  `site_web` varchar(50) DEFAULT NULL,
  `description_Societe` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nom_St`),
  UNIQUE KEY `id_Ste` (`id_Ste`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `societe`
--

LOCK TABLES `societe` WRITE;
/*!40000 ALTER TABLE `societe` DISABLE KEYS */;
INSERT INTO `societe` VALUES (4,'Auto Diff','edgea','eagaegegeg','ega','gaegea','gzaegea'),(7,'Cap Mada','','','','',''),(6,'SODIATRANS','jniodg','hidkl','gdikl','gdnklx','ilk'),(5,'TOA','huibjk','hdjk','bdjk','vbdjk','vlk');
/*!40000 ALTER TABLE `societe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systeme`
--

DROP TABLE IF EXISTS `systeme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systeme` (
  `id_Syst` int(11) NOT NULL AUTO_INCREMENT,
  `nom_Syst` varchar(50) NOT NULL,
  PRIMARY KEY (`nom_Syst`),
  UNIQUE KEY `id_Syst` (`id_Syst`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systeme`
--

LOCK TABLES `systeme` WRITE;
/*!40000 ALTER TABLE `systeme` DISABLE KEYS */;
INSERT INTO `systeme` VALUES (1,'Windows 7 Integral 64 bits'),(2,'Windows XP SP3'),(3,'Windows 8 Pro 64 bits'),(5,'Windows 8 Pro 32 bits');
/*!40000 ALTER TABLE `systeme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_User` int(11) NOT NULL AUTO_INCREMENT,
  `user_Names` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  PRIMARY KEY (`id_User`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Administrateur','12345','Administrateur','Admin'),(3,'Sitraka','1234','Sitr','Sitraka');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utilisateur` (
  `matricule` varchar(50) NOT NULL,
  `nom_util` varchar(50) NOT NULL,
  `prenom_util` varchar(50) NOT NULL,
  `tel_util` varchar(20) NOT NULL,
  `email_util` varchar(50) NOT NULL,
  `nom_St` varchar(50) NOT NULL,
  `nom_Depart` varchar(50) NOT NULL,
  PRIMARY KEY (`nom_util`),
  UNIQUE KEY `matricule` (`matricule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES ('qdgz','egz','ezg','hzeg','zehg','Auto Diff','Informatique'),('15','lova','landy','0334568875','landy@hotmail.com','Auto diffusion','Informatique'),('123456','Riana','Sitrakiniaina','0331245678','bhjfssbuijf@bsjkfq.com','Auto diffusion','Informatique'),('67','Sitraka','Riana','0330546132','sitraka@sodiat.mg','Cap Mada','Informatique');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-01 10:46:44
