
package InterfaceAdmin;

import ConnectionBDD.Connecter;
import static InterfaceAdmin.ListeMachine.tab;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;


public class ListeMachineIntervention extends javax.swing.JDialog {

    Connecter conn = new Connecter();

    ResultSet resultat1, resultat, rs;
    Statement stmt;
    
    public ListeMachineIntervention(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
                try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        RemplirCB("nom_Rep","reparateur",reparateur);
    }

    public void RemplirCB(String primaire, String table, JComboBox CB) {
        String req = "SELECT "+ primaire +" FROM  " + table + "";

        CB.removeAllItems();
        CB.addItem("");
        try {
            resultat = stmt.executeQuery(req);
            while (resultat.next()) {
                CB.addItem(resultat.getString(1));
            }
            resultat.close();
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        IntervForm = new javax.swing.JPanel();
        Debut = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        heurDebut = new javax.swing.JFormattedTextField();
        dateDebut = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        heurFin = new javax.swing.JFormattedTextField();
        dateFin = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        reparateur = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        desc = new javax.swing.JTextArea();
        Rep = new javax.swing.JButton();
        AjoutInterv = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        IntervForm.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Intervention", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        Debut.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Debut", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        jLabel48.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel48.setText("Date ");

        jLabel49.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel49.setText("Heure");

        try {
            heurDebut.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        heurDebut.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        heurDebut.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        dateDebut.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dateDebut.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        dateDebut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateDebutMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout DebutLayout = new javax.swing.GroupLayout(Debut);
        Debut.setLayout(DebutLayout);
        DebutLayout.setHorizontalGroup(
            DebutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DebutLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DebutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel48)
                    .addComponent(jLabel49))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(DebutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(heurDebut, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                    .addComponent(dateDebut))
                .addContainerGap())
        );
        DebutLayout.setVerticalGroup(
            DebutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DebutLayout.createSequentialGroup()
                .addGroup(DebutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel48)
                    .addComponent(dateDebut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(DebutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel49)
                    .addComponent(heurDebut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Fin", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        jLabel50.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel50.setText("Date ");

        jLabel51.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel51.setText("Heure ");

        try {
            heurFin.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        heurFin.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        heurFin.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        dateFin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dateFin.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        dateFin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateFinMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel50)
                    .addComponent(jLabel51))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(heurFin, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                    .addComponent(dateFin))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel50)
                    .addComponent(dateFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel51)
                    .addComponent(heurFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Reparateur");

        reparateur.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        reparateur.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        reparateur.setSelectedIndex(-1);

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Description");

        desc.setColumns(20);
        desc.setRows(5);
        jScrollPane6.setViewportView(desc);

        Rep.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        Rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/customize-512.png"))); // NOI18N
        Rep.setText("Intervenant");
        Rep.setToolTipText("");
        Rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RepActionPerformed(evt);
            }
        });

        AjoutInterv.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        AjoutInterv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Ajouter.png"))); // NOI18N
        AjoutInterv.setText("Ajouter l'intervention");
        AjoutInterv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AjoutIntervActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout IntervFormLayout = new javax.swing.GroupLayout(IntervForm);
        IntervForm.setLayout(IntervFormLayout);
        IntervFormLayout.setHorizontalGroup(
            IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, IntervFormLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(IntervFormLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(AjoutInterv))
                    .addGroup(IntervFormLayout.createSequentialGroup()
                        .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(Debut, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(IntervFormLayout.createSequentialGroup()
                                .addComponent(reparateur, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Rep))
                            .addComponent(jScrollPane6))))
                .addContainerGap())
        );
        IntervFormLayout.setVerticalGroup(
            IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(IntervFormLayout.createSequentialGroup()
                .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(IntervFormLayout.createSequentialGroup()
                        .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reparateur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Rep, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(IntervFormLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane6)))
                    .addGroup(IntervFormLayout.createSequentialGroup()
                        .addComponent(Debut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12)
                .addComponent(AjoutInterv))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(IntervForm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(IntervForm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void RepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RepActionPerformed
        try {
            Reparateur u = new Reparateur(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_Rep","reparateur",reparateur);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_RepActionPerformed

    private void AjoutIntervActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AjoutIntervActionPerformed
        //Ajoute une novelle intervention
        String requete = "insert into intervention(date_Debut_Interv,heur_Debut_Interv,date_Fin_Interv"
        + ",heur_Fin_Interv,nom_Rep,description_Interv,id_Machine)VALUES('"
        + dateDebut.getText() + "','"
        + heurDebut.getText() + "','"
        + dateFin.getText() + "','"
        + heurFin.getText() + "','"
        + reparateur.getSelectedItem().toString() + "','"
        + desc.getText() + "','"
        + tab.getValueAt(tab.getSelectedRow(), 0) + "')";

        try {
            stmt.executeUpdate(requete);
            JOptionPane.showMessageDialog(null, "Ajout d'intervention");
            dispose();
        } catch (Exception ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_AjoutIntervActionPerformed

    private void dateDebutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateDebutMouseClicked
        Calendrier c = new Calendrier(new javax.swing.JFrame(), true);
        c.setVisible(true);
        if (c.isShowing() == false) {
            c.dateTxt(dateDebut);
        }
        
    }//GEN-LAST:event_dateDebutMouseClicked

    private void dateFinMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateFinMouseClicked
        Calendrier c = new Calendrier(new javax.swing.JFrame(), true);
        c.setVisible(true);
        if (c.isShowing() == false) {
            c.dateTxt(dateFin);
        }
    }//GEN-LAST:event_dateFinMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListeMachineIntervention.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListeMachineIntervention.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListeMachineIntervention.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListeMachineIntervention.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ListeMachineIntervention dialog = new ListeMachineIntervention(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton AjoutInterv;
    private javax.swing.JPanel Debut;
    private javax.swing.JPanel IntervForm;
    private javax.swing.JButton Rep;
    private javax.swing.JTextField dateDebut;
    private javax.swing.JTextField dateFin;
    private javax.swing.JTextArea desc;
    private javax.swing.JFormattedTextField heurDebut;
    private javax.swing.JFormattedTextField heurFin;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane6;
    public static javax.swing.JComboBox<String> reparateur;
    // End of variables declaration//GEN-END:variables
}
