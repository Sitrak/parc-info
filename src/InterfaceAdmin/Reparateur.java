
package InterfaceAdmin;

import ConnectionBDD.Connecter;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public final class Reparateur extends javax.swing.JDialog {
    
    Connecter conn = new Connecter();

    ResultSet resultat;
    Statement stmt;
    DefaultTableModel model = new DefaultTableModel();
    
    public Reparateur(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();        
        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
               
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));       
               
        //Desactiver les bouton
        modifier.setEnabled(false);
        supprimer.setEnabled(false);
        vider.setEnabled(false);
        
        //Ajouter les colonnes
        model.addColumn("Matricule");
        model.addColumn("Nom du reparateur");
        model.addColumn("Prenom du reparateur");
        model.addColumn("Telephone");
        model.addColumn("Email");
        resultat = stmt.executeQuery("Select * from reparateur");
        Actualiser(resultat);
    }
    public void Deplacer(int i) {
        //Deplacer le contenu des lignes dans les champs de text
        try {
            matricule.setText(model.getValueAt(i, 0).toString());
            nom.setText(model.getValueAt(i, 1).toString());
            prenom.setText(model.getValueAt(i, 2).toString());
            tel.setText(model.getValueAt(i,3).toString());
            mail.setText(model.getValueAt(i, 4).toString());
        } catch (Exception e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    public void Actualiser(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    resultat.getString("matricul_Rep"),
                    resultat.getString("nom_Rep"),
                    resultat.getString("prenom_Rep"),
                    resultat.getString("tel_Rep"),
                    resultat.getString("email_Rep")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tab.setModel(model);
    }
    
        private void ViderLeChamp() {
        //Vider le champ de test        
        matricule.setText("");
        nom.setText("");
        prenom.setText("");
        tel.setText("");
        mail.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Formulaire = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        matricule = new javax.swing.JTextField();
        nom = new javax.swing.JTextField();
        prenom = new javax.swing.JTextField();
        mail = new javax.swing.JTextField();
        tel = new javax.swing.JTextField();
        ajouter = new javax.swing.JButton();
        vider = new javax.swing.JButton();
        modifier = new javax.swing.JButton();
        supprimer = new javax.swing.JButton();
        fermer = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Reparateur - Administrateur");

        Formulaire.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder()));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel1.setText("Matricule");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Nom");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Prénoms");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Tél");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setText("Email");

        matricule.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        nom.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        prenom.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        mail.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        tel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        ajouter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ajouter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Ajouter.png"))); // NOI18N
        ajouter.setText("Ajouter");
        ajouter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ajouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterActionPerformed(evt);
            }
        });

        vider.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        vider.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Vider.png"))); // NOI18N
        vider.setText("Vider les champs");
        vider.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        vider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viderActionPerformed(evt);
            }
        });

        modifier.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });

        supprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        supprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Supprimer.png"))); // NOI18N
        supprimer.setText("Supprimer");
        supprimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        supprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerActionPerformed(evt);
            }
        });

        fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        fermer.setText("Fermer");
        fermer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fermerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormulaireLayout = new javax.swing.GroupLayout(Formulaire);
        Formulaire.setLayout(FormulaireLayout);
        FormulaireLayout.setHorizontalGroup(
            FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormulaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(FormulaireLayout.createSequentialGroup()
                        .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(mail, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                            .addComponent(tel)
                            .addComponent(nom)
                            .addComponent(matricule)
                            .addComponent(prenom, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fermer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(supprimer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(modifier, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ajouter, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(vider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        FormulaireLayout.setVerticalGroup(
            FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormulaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ajouter))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifier))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(supprimer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(vider))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(mail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fermer))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Formulaire, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 485, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Formulaire, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void ajouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajouterActionPerformed
        String requete = "insert into reparateur("
        + "matricul_Rep,nom_Rep,prenom_Rep,tel_Rep,email_Rep)VALUES('"
        + matricule.getText() + "','"
        + nom.getText() + "','"
        + prenom.getText() + "','"
        + tel.getText() + "','"
        + mail.getText() + "')";
        if (nom.equals("")) {
            JOptionPane.showMessageDialog(null, "Veullez ramplir tous les champs");
        } else {
            try {
                stmt.executeUpdate(requete);
                resultat = stmt.executeQuery("Select * from reparateur");
                Actualiser(resultat);
                ViderLeChamp();
            } catch (Exception ex) {
                System.err.println(ex);
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }//GEN-LAST:event_ajouterActionPerformed

    private void viderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viderActionPerformed
       ViderLeChamp();
       //Desactiver les bouton
        modifier.setEnabled(false);
        supprimer.setEnabled(false);
    }//GEN-LAST:event_viderActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();

        String update = "update  reparateur set "
        + "matricul_Rep = '" + matricule.getText() + "',"
        + "nom_Rep = '" + nom.getText() + "',"
        + "prenom_Rep = '" + prenom.getText() + "',"
        + "tel_Rep = '" + tel.getText() + "',"
        + "email_Rep = '" + mail.getText() + "' where matricul_Rep = '" + selectionner + "'";

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez modifier cette reparateur ?", "Confirmation de modification",
                JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
            stmt.executeUpdate(update);
            resultat = stmt.executeQuery("Select * from reparateur");
            Actualiser(resultat);
            ViderLeChamp();
        }
        } catch (Exception ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_modifierActionPerformed

    private void supprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerActionPerformed
        String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment supprimer cette reparateur ?", "Confirmation de suppression",
                JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
            model.removeRow(tab.getSelectedRow());
            stmt.executeUpdate("DELETE FROM reparateur WHERE  matricul_Rep = '" + selectionner +"'"   );
            resultat = stmt.executeQuery("Select * from reparateur");
            Actualiser(resultat);
        }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, e);
        } catch (SQLException ex) {
            Logger.getLogger(Reparateur.class.getName()).log(Level.SEVERE, null, ex);
        }
        ViderLeChamp();
    }//GEN-LAST:event_supprimerActionPerformed

    private void fermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fermerActionPerformed
        dispose();
    }//GEN-LAST:event_fermerActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        int i = tab.getSelectedRow();
        Deplacer(i);
        modifier.setEnabled(true);
        supprimer.setEnabled(true);
        vider.setEnabled(true);
    }//GEN-LAST:event_tabMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Reparateur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Reparateur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Reparateur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Reparateur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Reparateur dialog = new Reparateur(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Reparateur.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Formulaire;
    private javax.swing.JButton ajouter;
    private javax.swing.JButton fermer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField mail;
    private javax.swing.JTextField matricule;
    private javax.swing.JButton modifier;
    private javax.swing.JTextField nom;
    private javax.swing.JTextField prenom;
    private javax.swing.JButton supprimer;
    private javax.swing.JTable tab;
    private javax.swing.JTextField tel;
    private javax.swing.JButton vider;
    // End of variables declaration//GEN-END:variables
}
