package InterfaceAdmin;

import ConnectionBDD.Connecter;
import static InterfaceAdmin.ListeMachineFormulaire.ValiderModification;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public final class ListeMachine extends javax.swing.JDialog {

    Connecter conn = new Connecter();
    
    ResultSet resultat1,resultat,rs;
    Statement stmt,stmt2;
    DefaultTableModel model = new DefaultTableModel();
    DefaultTableModel model2 = new DefaultTableModel();
      
    public ListeMachine(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();

        try {
            stmt = conn.obtenirConnexion().createStatement();
            stmt2 = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));

        //desactiver les bouttons
        Modifier.setEnabled(false);
        Supprimer.setEnabled(false);
        btAjoutInterv.setEnabled(false);
        BtPrintInterv.setEnabled(false);
        ExportInterv.setEnabled(false);


        //Ajout des colonnes liste
        model.addColumn("id");
        model.addColumn("Situation");
        model.addColumn("Type");
        model.addColumn("Nom du Machine");
        model.addColumn("Nom de la Marque");
        model.addColumn("Nom de l'utilisateur");
        model.addColumn("Lieu");
        model.addColumn("Société");
        model.addColumn("Adresse IP");
        model.addColumn("Fournisseur");
        model.addColumn("Date d'Achat");
        model.addColumn("Duree de la Garantie");
        model.addColumn("Prix du Machine");
        model.addColumn("Modele du Machine");
        model.addColumn("Systeme");
        model.addColumn("Processeur");
        model.addColumn("RAM");
        model.addColumn("HDD");
        model.addColumn("Ecran");
        model.addColumn("Autre description");

        //Afficher tous les machines au demarrage
        resultat1 = stmt.executeQuery("Select * from machine");
        Actualiser(resultat1);

        //Ajout de colonne dans le table intervention
        model2.addColumn("Date de Debut");
        model2.addColumn("Heure de debut");
        model2.addColumn("Date de fin");
        model2.addColumn("Heure de fin");
        model2.addColumn("Reparateur");
        model2.addColumn("Descrition");
        tabInterv.setModel(model2);
        

        Compter("type", "station", nbrStation);
        Compter("type", "portable", nbrPortable);
        Compter("situation", "en service", nbrEnService);
        Compter("situation", "en stock", nbrStock);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtGp = new javax.swing.ButtonGroup();
        Type = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        Tableau = new javax.swing.JPanel();
        Intervention = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabInterv = new javax.swing.JTable();
        Liste = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        Boutton = new javax.swing.JPanel();
        Nouveau = new javax.swing.JButton();
        Modifier = new javax.swing.JButton();
        Supprimer = new javax.swing.JButton();
        Fermer = new javax.swing.JButton();
        Imprimer = new javax.swing.JButton();
        ExportMach = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        btAjoutInterv = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        BtPrintInterv = new javax.swing.JButton();
        ExportInterv = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        txtRecherche = new javax.swing.JTextField();
        Chercher = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        Portable = new javax.swing.JRadioButton();
        Station = new javax.swing.JRadioButton();
        MachineEnStock = new javax.swing.JRadioButton();
        ToutMachine = new javax.swing.JRadioButton();
        MachineEnService = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        nbrStation = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        nbrPortable = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        nbrEnService = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nbrStock = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Listes des ordinateurs - Administrateur");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        Tableau.setToolTipText("");

        Intervention.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Intervention", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 12))); // NOI18N

        tabInterv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tabInterv);

        javax.swing.GroupLayout InterventionLayout = new javax.swing.GroupLayout(Intervention);
        Intervention.setLayout(InterventionLayout);
        InterventionLayout.setHorizontalGroup(
            InterventionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
        );
        InterventionLayout.setVerticalGroup(
            InterventionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE)
        );

        Liste.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Liste des Machines", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 12))); // NOI18N

        tab.setAutoCreateRowSorter(true);
        tab.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        TableColumn col = tab.getColumnModel().getColumn(0);
        col.setPreferredWidth(200);
        tab.setEditingColumn(0);
        tab.setEditingRow(0);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tab);

        javax.swing.GroupLayout ListeLayout = new javax.swing.GroupLayout(Liste);
        Liste.setLayout(ListeLayout);
        ListeLayout.setHorizontalGroup(
            ListeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
        );
        ListeLayout.setVerticalGroup(
            ListeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        Nouveau.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Nouveau.png"))); // NOI18N
        Nouveau.setText("Nouveau");
        Nouveau.setToolTipText("Entrer une nouvelle ordinateur");
        Nouveau.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NouveauActionPerformed(evt);
            }
        });

        Modifier.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        Modifier.setText("Modifier");
        Modifier.setToolTipText("Modifier l'ordinateur selectionner");
        Modifier.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModifierActionPerformed(evt);
            }
        });

        Supprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Supprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Supprimer.png"))); // NOI18N
        Supprimer.setText("Supprimer");
        Supprimer.setToolTipText("Supprimer l'ordinateur selectionner");
        Supprimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Supprimer.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        Supprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SupprimerActionPerformed(evt);
            }
        });

        Fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        Fermer.setText("Fermer");
        Fermer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FermerActionPerformed(evt);
            }
        });

        Imprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Imprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Imprimer.png"))); // NOI18N
        Imprimer.setText("Imprimer");
        Imprimer.setToolTipText("Imprimer le tableau");
        Imprimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Imprimer.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        Imprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImprimerActionPerformed(evt);
            }
        });

        ExportMach.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ExportMach.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/export.png"))); // NOI18N
        ExportMach.setText("Exporter");
        ExportMach.setToolTipText("Exporter le tableau sous excel");
        ExportMach.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ExportMach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExportMachActionPerformed(evt);
            }
        });

        btAjoutInterv.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btAjoutInterv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Ajouter.png"))); // NOI18N
        btAjoutInterv.setText("Ajouter");
        btAjoutInterv.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btAjoutInterv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAjoutIntervActionPerformed(evt);
            }
        });

        BtPrintInterv.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        BtPrintInterv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Imprimer.png"))); // NOI18N
        BtPrintInterv.setText("Imprimer");
        BtPrintInterv.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        BtPrintInterv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtPrintIntervActionPerformed(evt);
            }
        });

        ExportInterv.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ExportInterv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/export.png"))); // NOI18N
        ExportInterv.setText("Exporter");
        ExportInterv.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout BouttonLayout = new javax.swing.GroupLayout(Boutton);
        Boutton.setLayout(BouttonLayout);
        BouttonLayout.setHorizontalGroup(
            BouttonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BouttonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(BouttonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Fermer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Supprimer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Nouveau, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Modifier, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Imprimer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ExportMach, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btAjoutInterv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BtPrintInterv, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ExportInterv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator3)
                    .addComponent(jSeparator4))
                .addContainerGap())
        );
        BouttonLayout.setVerticalGroup(
            BouttonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BouttonLayout.createSequentialGroup()
                .addComponent(Nouveau, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Modifier, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Supprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Imprimer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ExportMach)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Fermer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 260, Short.MAX_VALUE)
                .addComponent(btAjoutInterv)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 3, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BtPrintInterv)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ExportInterv)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 12))); // NOI18N

        txtRecherche.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtRecherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRechercheKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRechercheKeyTyped(evt);
            }
        });

        Chercher.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Chercher.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/recherher.png"))); // NOI18N
        Chercher.setText("Rechercher");
        Chercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChercherActionPerformed(evt);
            }
        });
        Chercher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ChercherKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtRecherche, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Chercher)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Chercher))
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        buttonGroup1.add(Portable);
        Portable.setText("Portable");
        Portable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PortableActionPerformed(evt);
            }
        });

        buttonGroup1.add(Station);
        Station.setText("Station");
        Station.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StationActionPerformed(evt);
            }
        });

        buttonGroup1.add(MachineEnStock);
        MachineEnStock.setText("En Stock");
        MachineEnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MachineEnStockActionPerformed(evt);
            }
        });

        buttonGroup1.add(ToutMachine);
        ToutMachine.setSelected(true);
        ToutMachine.setText("Tous");
        ToutMachine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ToutMachineActionPerformed(evt);
            }
        });

        buttonGroup1.add(MachineEnService);
        MachineEnService.setText("En Service");
        MachineEnService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MachineEnServiceActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(ToutMachine)
                .addGap(50, 50, 50)
                .addComponent(Portable)
                .addGap(49, 49, 49)
                .addComponent(Station)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addComponent(MachineEnService)
                .addGap(40, 40, 40)
                .addComponent(MachineEnStock)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(MachineEnStock)
                .addComponent(ToutMachine)
                .addComponent(Portable)
                .addComponent(Station)
                .addComponent(MachineEnService))
        );

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Station : ");

        nbrStation.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        nbrStation.setText("nbr");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Portable : ");

        nbrPortable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        nbrPortable.setText("nbr");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("En service : ");

        nbrEnService.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        nbrEnService.setText("nbr");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("En stock : ");

        nbrStock.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        nbrStock.setText("nbr");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nbrStation, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nbrPortable, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nbrEnService, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nbrStock, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(nbrStation, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(nbrPortable, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(nbrEnService, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(nbrStock, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout TableauLayout = new javax.swing.GroupLayout(Tableau);
        Tableau.setLayout(TableauLayout);
        TableauLayout.setHorizontalGroup(
            TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableauLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Intervention, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(TableauLayout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(Liste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Boutton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        TableauLayout.setVerticalGroup(
            TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableauLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TableauLayout.createSequentialGroup()
                        .addComponent(Boutton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(19, 19, 19))
                    .addGroup(TableauLayout.createSequentialGroup()
                        .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Liste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Intervention, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tableau, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tableau, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    //Compter les ligne
    public void Compter(String colonne, String contenu, JLabel label) {
        String req = "select count(*) from machine where " + colonne + " ='" + contenu + "'";
        try {
            resultat = stmt.executeQuery(req);
            resultat.next();
            label.setText(resultat.getString(1));
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    public void Actualiser(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    resultat.getString("id_Machine"),
                    resultat.getString("situation"),
                    resultat.getString("type"),
                    resultat.getString("nom_Machine"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("nom_util"),
                    resultat.getString("lieu"),
                    resultat.getString("nom_St"),
                    resultat.getString("adresse_IP"),
                    resultat.getString("nom_Fournisseur"),
                    resultat.getString("date_Achat"),
                    resultat.getString("duree_Garantie"),
                    resultat.getString("prix_Machine"),
                    resultat.getString("modele_Machine"),
                    resultat.getString("nom_Syst"),
                    resultat.getString("cpu"),
                    resultat.getString("ram"),
                    resultat.getString("hdd"),
                    resultat.getString("ecran"),
                    resultat.getString("autre_Description"),
                    resultat.getString("id_Marque"),
                    resultat.getString("id_Fournisseur"),});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tab.setModel(model);
    }

    public void ActualiserInterv() {
        try {

            model2.setRowCount(0);//Vider le colonne avant l'affichage
            resultat = stmt.executeQuery("Select * from intervention where id_Machine =" + tab.getValueAt(tab.getSelectedRow(), 0));

            while (resultat.next()) {
                model2.addRow(new Object[]{
                    resultat.getString("date_Debut_Interv"),
                    resultat.getString("heur_Debut_Interv"),
                    resultat.getString("date_Fin_Interv"),
                    resultat.getString("heur_Fin_Interv"),
                    resultat.getString("nom_Rep"),
                    resultat.getString("description_Interv"),
                    resultat.getString("id_Interv"),
                    resultat.getString("id_Machine")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tabInterv.setModel(model2);
    }

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void ChercherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ChercherKeyPressed

    }//GEN-LAST:event_ChercherKeyPressed

    private void ChercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChercherActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from machine where "
                    + "nom_Machine = '" + txtRecherche.getText() + "'"
                    + "or nom_Marque = '" + txtRecherche.getText() + "'"
                    + "or nom_util = '" + txtRecherche.getText() + "'"
                    + "or lieu = '" + txtRecherche.getText() + "'"
                    + "or nom_St = '" + txtRecherche.getText() + "'"
                    + "or adresse_Ip = '" + txtRecherche.getText() + "'"
                    + "or nom_Fournisseur = '" + txtRecherche.getText() + "'"
                    + "or date_Achat = '" + txtRecherche.getText() + "'"
                    + "or duree_Garantie = '" + txtRecherche.getText() + "'"
                    + "or prix_Machine = '" + txtRecherche.getText() + "'"
                    + "or cpu = '" + txtRecherche.getText() + "'"
                    + "or hdd = '" + txtRecherche.getText() + "'"
                    + "or ecran = '" + txtRecherche.getText() + "'"
                    + "or ram = '" + txtRecherche.getText() + "'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ChercherActionPerformed

    private void txtRechercheKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyTyped

    }//GEN-LAST:event_txtRechercheKeyTyped

    private void txtRechercheKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyPressed

    }//GEN-LAST:event_txtRechercheKeyPressed

    private void ImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImprimerActionPerformed
        MessageFormat header = new MessageFormat("Liste des ordinateurs");
        MessageFormat footer = new MessageFormat("");
        try {
            tab.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } catch (PrinterException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ImprimerActionPerformed

    private void FermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FermerActionPerformed
        dispose();
    }//GEN-LAST:event_FermerActionPerformed

    //Supression de ligne
    private void SupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SupprimerActionPerformed

        if (tab.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionnez une machine");
        } else {
            String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();
            try {
                if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment supprimer cette machine ?", "Supprimer Machine",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                    model.removeRow(tab.getSelectedRow());
                    stmt.executeUpdate("DELETE FROM machine WHERE  id_Machine = '" + selectionner + "'");

                    resultat = stmt.executeQuery("Select * from machine");
                    Actualiser(resultat);
                    Compter("type", "station", nbrStation);
                    Compter("type", "portable", nbrPortable);
                    Compter("situation", "en service", nbrEnService);
                    Compter("situation", "en stock", nbrStock);
                    Modifier.setEnabled(false);
                    Supprimer.setEnabled(false);
                }
            } catch (HeadlessException | SQLException e) {
                System.err.println(e);
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_SupprimerActionPerformed

    private void ModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModifierActionPerformed
            ListeMachineFormulaire i = new ListeMachineFormulaire(new javax.swing.JFrame(), true);
            i.AfficheContenu();
            i.setVisible(true);
        if (i.isShowing() == false) {
                try {
                    resultat1 = stmt2.executeQuery("Select * from machine");
                    Compter("type", "station", nbrStation);
                    Compter("type", "portable", nbrPortable);
                    Compter("situation", "en service", nbrEnService);
                    Compter("situation", "en stock", nbrStock);
                    Actualiser(resultat1);
                    Modifier.setEnabled(false);
                    Supprimer.setEnabled(false);
                    btAjoutInterv.setEnabled(false);
                } catch (SQLException ex) {
                    Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }//GEN-LAST:event_ModifierActionPerformed

    private void NouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NouveauActionPerformed
        ListeMachineFormulaire i = new ListeMachineFormulaire(new javax.swing.JFrame(), true);
         i.setVisible(true);
         ValiderModification.setEnabled(false);
         if (i.isShowing() == false) {
            try {
                resultat1 = stmt.executeQuery("Select * from machine");
                
                Compter("type", "station", nbrStation);
                Compter("type", "portable", nbrPortable);
                Compter("situation", "en service", nbrEnService);
                Compter("situation", "en stock", nbrStock);
                Actualiser(resultat1);
                Modifier.setEnabled(false);
                Supprimer.setEnabled(false);
                btAjoutInterv.setEnabled(false);
            } catch (SQLException ex) {
                Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
       
    }//GEN-LAST:event_NouveauActionPerformed

    private void PortableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PortableActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from machine where type = 'Portable'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_PortableActionPerformed

    private void StationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StationActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from machine where type = 'Station'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_StationActionPerformed

    private void MachineEnStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MachineEnStockActionPerformed

        try {
            resultat = stmt.executeQuery("Select * from machine where situation = 'En Stock'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MachineEnStockActionPerformed

    private void ToutMachineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ToutMachineActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from machine");
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
        Actualiser(resultat);
    }//GEN-LAST:event_ToutMachineActionPerformed

    private void MachineEnServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MachineEnServiceActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from machine where situation = 'En Service'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MachineEnServiceActionPerformed

    private void btAjoutIntervActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAjoutIntervActionPerformed
        ListeMachineIntervention l = new ListeMachineIntervention(new javax.swing.JFrame(), true);
        l.setVisible(true);
        if (l.isShowing() == false) {
            ActualiserInterv();
            Modifier.setEnabled(false);
            Supprimer.setEnabled(false);
            btAjoutInterv.setEnabled(false);
        }
    }//GEN-LAST:event_btAjoutIntervActionPerformed

    private void BtPrintIntervActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtPrintIntervActionPerformed
        MessageFormat header = new MessageFormat("Report Print");
        MessageFormat footer = new MessageFormat("page(0,number,integer)");

        try {
            tab.print(JTable.PrintMode.NORMAL, header, footer);
        } catch (PrinterException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtPrintIntervActionPerformed

    private void ExportMachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExportMachActionPerformed
        ExporterExcel.exporter(this, tab);
    }//GEN-LAST:event_ExportMachActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        Modifier.setEnabled(true);
        Supprimer.setEnabled(true);
        btAjoutInterv.setEnabled(true);
        BtPrintInterv.setEnabled(true);
        ExportInterv.setEnabled(true);        
   
        ActualiserInterv();
    }//GEN-LAST:event_tabMouseClicked

    public static void main(String args[]) throws SQLException {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListeMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListeMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListeMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListeMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ListeMachine dialog = new ListeMachine(new javax.swing.JFrame(), true);

                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Boutton;
    private javax.swing.ButtonGroup BtGp;
    private javax.swing.JButton BtPrintInterv;
    private javax.swing.JButton Chercher;
    private javax.swing.JButton ExportInterv;
    private javax.swing.JButton ExportMach;
    private javax.swing.JButton Fermer;
    private javax.swing.JButton Imprimer;
    private javax.swing.JPanel Intervention;
    private javax.swing.JPanel Liste;
    private javax.swing.JRadioButton MachineEnService;
    private javax.swing.JRadioButton MachineEnStock;
    public static javax.swing.JButton Modifier;
    private javax.swing.JButton Nouveau;
    private javax.swing.JRadioButton Portable;
    private javax.swing.JRadioButton Station;
    public static javax.swing.JButton Supprimer;
    public static javax.swing.JPanel Tableau;
    private javax.swing.JRadioButton ToutMachine;
    private javax.swing.ButtonGroup Type;
    private javax.swing.JButton btAjoutInterv;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel nbrEnService;
    private javax.swing.JLabel nbrPortable;
    private javax.swing.JLabel nbrStation;
    private javax.swing.JLabel nbrStock;
    public static javax.swing.JTable tab;
    private javax.swing.JTable tabInterv;
    private javax.swing.JTextField txtRecherche;
    // End of variables declaration//GEN-END:variables

}
