package InterfaceAdmin;

import ConnectionBDD.Connecter;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public final class etat extends javax.swing.JDialog {

    Connecter conn = new Connecter();
    ResultSet resultat,rs;
    
    DefaultTableModel modelMachine = new DefaultTableModel();
    DefaultTableModel modelLog = new DefaultTableModel();
    DefaultTableModel modelMat = new DefaultTableModel();
    Statement stmt;

    public etat(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();

        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));

        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        
        remplirComboBox_Ste();
        
        //Ajout des colonnes machine
        modelMachine.addColumn("Nom du Machine");
        modelMachine.addColumn("Nom de l'utilisateur");
        modelMachine.addColumn("Type");
        modelMachine.addColumn("Société");
        modelMachine.addColumn("Lieu");
        modelMachine.addColumn("Situation");
        modelMachine.addColumn("Nom de la Marque");
        modelMachine.addColumn("Ecran");
        modelMachine.addColumn("Fournisseur");
        modelMachine.addColumn("Date d'Achat");
        modelMachine.addColumn("Duree de la Garantie");
        modelMachine.addColumn("Prix du Machine");
        modelMachine.addColumn("Processeur");
        modelMachine.addColumn("RAM");
        modelMachine.addColumn("HDD");
        modelMachine.addColumn("Autre description");

        resultat = stmt.executeQuery("Select * from machine");
        ActualiserMach(resultat);

        
        //Ajout des colonnes Logiciel 
        modelLog.addColumn("Nom du Logiciel");
        modelLog.addColumn("Type");
        modelLog.addColumn("Nom marque");
        modelLog.addColumn("Date achat");
        modelLog.addColumn("Prix d'achat");
        modelLog.addColumn("Societe");
        modelLog.addColumn("Description");

        resultat = stmt.executeQuery("Select * from logiciel");
        ActualiserLog(resultat);
        
        //Ajout des colonnes Materiel
        modelMat.addColumn("Nom Materiel");
        modelMat.addColumn("Marque");
        modelMat.addColumn("Societe");
        modelMat.addColumn("Fournisseur");
        modelMat.addColumn("Date achat");
        modelMat.addColumn("Duree garantie");
        modelMat.addColumn("Prix achat");
        modelMat.addColumn("Situation");
        modelMat.addColumn("Description");
        
        resultat = stmt.executeQuery("Select * from materiel");
        ActualiserMat(resultat);
    }
    public void ActualiserMach(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            modelMachine.setRowCount(0);

            while (resultat.next()) {
                modelMachine.addRow(new Object[]{
                    resultat.getString("nom_Machine"),
                    resultat.getString("nom_util"),
                    resultat.getString("type"),
                    resultat.getString("nom_St"),
                    resultat.getString("lieu"),
                    resultat.getString("situation"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("ecran"),
                    resultat.getString("nom_Fournisseur"),
                    resultat.getString("date_Achat"),
                    resultat.getString("duree_Garantie"),
                    resultat.getString("prix_Machine"),                  
                    resultat.getString("cpu"),
                    resultat.getString("ram"),
                    resultat.getString("hdd"),
                    resultat.getString("autre_Description")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tabMach.setModel(modelMachine);
    }
    public void ActualiserLog(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            modelLog.setRowCount(0);

            while (resultat.next()) {
                modelLog.addRow(new Object[]{
                    resultat.getString("nom_log"),
                    resultat.getString("type_Log"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("date_Achat"),
                    resultat.getString("prix_Achat"),
                    resultat.getString("nom_St"),
                    resultat.getString("description_Log")});
            }

        } catch (Exception e) {
            System.err.println(e);
        }
        tabLog.setModel(modelLog);
    }

    public void ActualiserMat(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            modelMat.setRowCount(0);

            while (resultat.next()) {
                modelMat.addRow(new Object[]{
                    resultat.getString("nom_Mat"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("nom_St"),
                    resultat.getString("nom_Fournisseur"),
                    resultat.getString("date_Achat_Mat"),
                    resultat.getString("duree_Garantie_Mat"),
                    resultat.getString("prix_Mat"),
                    resultat.getString("mat_En_Stock"),
                    resultat.getString("description_Mat")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tabMat.setModel(modelMat);
    }
    
    public void remplirComboBox_Ste() {
        String req = "SELECT nom_St FROM  societe";

        CBSte.removeAllItems();
        CBSte.addItem("Tous");
        try {
            rs = stmt.executeQuery(req);
            while (rs.next()) {
                CBSte.addItem(rs.getString(1));
            }
            rs.close();
        } catch (SQLException e) {
            System.err.println(e);
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        OngletOrdi = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabMach = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabMat = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabLog = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        CBSte = new javax.swing.JComboBox<>();
        Imprimer = new javax.swing.JButton();
        Exporter = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Etat - Administrateur");

        OngletOrdi.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        tabMach.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabMach.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane2.setViewportView(tabMach);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1003, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
        );

        OngletOrdi.addTab("Liste des Ordinateurs", jPanel1);

        tabMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabMat);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1003, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
        );

        OngletOrdi.addTab("Liste des Matériels", jPanel2);

        tabLog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tabLog);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1003, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
        );

        OngletOrdi.addTab("Liste des Logiciels", jPanel3);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Societe.png"))); // NOI18N
        jLabel1.setText("Sociéte");

        CBSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        CBSte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CBSteActionPerformed(evt);
            }
        });

        Imprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Imprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Imprimer.png"))); // NOI18N
        Imprimer.setText("Imprimer");
        Imprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImprimerActionPerformed(evt);
            }
        });

        Exporter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Exporter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/export.png"))); // NOI18N
        Exporter.setText("Exporter");
        Exporter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExporterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(OngletOrdi)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CBSte, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Exporter)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Imprimer)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(CBSte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Imprimer)
                    .addComponent(Exporter))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OngletOrdi)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void CBSteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CBSteActionPerformed
    try {
        if (CBSte.getSelectedItem().equals("Tous")) {
            
                resultat = stmt.executeQuery("Select * from machine");
                ActualiserMach(resultat);
                
                resultat = stmt.executeQuery("Select * from logiciel");
                ActualiserLog(resultat);
                
                resultat = stmt.executeQuery("Select * from materiel");
                ActualiserMat(resultat);
        }else{
                resultat = stmt.executeQuery("Select * from machine where nom_St = '"+ CBSte.getSelectedItem() +"'");
                ActualiserMach(resultat);
                
                resultat = stmt.executeQuery("Select * from logiciel where nom_St = '"+ CBSte.getSelectedItem() +"'");
                ActualiserLog(resultat);
                
                resultat = stmt.executeQuery("Select * from materiel where nom_St = '"+ CBSte.getSelectedItem() +"'");
                ActualiserMat(resultat);
        }
        } catch (SQLException ex) {
                Logger.getLogger(etat.class.getName()).log(Level.SEVERE, null, ex);
            }
    }//GEN-LAST:event_CBSteActionPerformed

    private void ExporterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExporterActionPerformed
        ChoixExportation u = new ChoixExportation(new javax.swing.JFrame(), true);
        u.setVisible(true);      
    }//GEN-LAST:event_ExporterActionPerformed

    private void ImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImprimerActionPerformed
        ChoixImpression u = new ChoixImpression(new javax.swing.JFrame(), true);
        u.setVisible(true);   
    }//GEN-LAST:event_ImprimerActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(etat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(etat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(etat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(etat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    etat dialog = new etat(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(etat.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CBSte;
    private javax.swing.JButton Exporter;
    private javax.swing.JButton Imprimer;
    private javax.swing.JTabbedPane OngletOrdi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    public static javax.swing.JTable tabLog;
    public static javax.swing.JTable tabMach;
    public static javax.swing.JTable tabMat;
    // End of variables declaration//GEN-END:variables
}
