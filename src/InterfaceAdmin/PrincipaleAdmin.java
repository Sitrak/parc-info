package InterfaceAdmin;

import Login.Login;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class PrincipaleAdmin extends javax.swing.JFrame {

    public PrincipaleAdmin() {
        initComponents();
        ChangerIcon();
        addWindowListener(new ecouteur());
    }

    public void ChangerIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        Deconnecter = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        quitter = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        ListeUtil = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        lstordinateur = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        lstmateriel = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        lstecran = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        lstlogiciel = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        stmexploitation = new javax.swing.JMenuItem();
        jMenu8 = new javax.swing.JMenu();
        Societe = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        Depart = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        sauv = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        restauration = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        marque = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        fournisseur = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        jMenuItem4 = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Gestion du parc Informatique - Administrateur");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/slide_sodiat.jpg"))); // NOI18N
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jMenu1.setText("Fichier");
        jMenu1.add(jSeparator6);

        Deconnecter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/logout-xxl.png"))); // NOI18N
        Deconnecter.setText("Deconnexion");
        Deconnecter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DeconnecterActionPerformed(evt);
            }
        });
        jMenu1.add(Deconnecter);
        jMenu1.add(jSeparator3);

        quitter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Action-exit-icon.png"))); // NOI18N
        quitter.setText("Quitter");
        quitter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitterActionPerformed(evt);
            }
        });
        jMenu1.add(quitter);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Utilisateur");

        ListeUtil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Utilisateur.png"))); // NOI18N
        ListeUtil.setText("Liste des utilisateurs");
        ListeUtil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ListeUtilActionPerformed(evt);
            }
        });
        jMenu4.add(ListeUtil);
        jMenu4.add(jSeparator4);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/login.png"))); // NOI18N
        jMenuItem2.setText("Users");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem2);

        jMenuBar1.add(jMenu4);

        jMenu5.setText("Materiel");

        lstordinateur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Devices-computer-icon.png"))); // NOI18N
        lstordinateur.setText("Liste Des Ordinateurs");
        lstordinateur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lstordinateurActionPerformed(evt);
            }
        });
        jMenu5.add(lstordinateur);
        jMenu5.add(jSeparator1);

        lstmateriel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/access_point.png"))); // NOI18N
        lstmateriel.setText("Liste Des Matériels");
        lstmateriel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lstmaterielActionPerformed(evt);
            }
        });
        jMenu5.add(lstmateriel);
        jMenu5.add(jSeparator5);

        lstecran.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/lcd_monitor.png"))); // NOI18N
        lstecran.setText("Liste Des Ecrans");
        lstecran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lstecranActionPerformed(evt);
            }
        });
        jMenu5.add(lstecran);

        jMenuBar1.add(jMenu5);

        jMenu6.setText("Logiciel");

        lstlogiciel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/software-installation-icons-36313.png"))); // NOI18N
        lstlogiciel.setText("Liste Des Logiciels");
        lstlogiciel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lstlogicielActionPerformed(evt);
            }
        });
        jMenu6.add(lstlogiciel);
        jMenu6.add(jSeparator9);

        stmexploitation.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/téléchargement-(4).png"))); // NOI18N
        stmexploitation.setText("Système d'Exploitation");
        stmexploitation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stmexploitationActionPerformed(evt);
            }
        });
        jMenu6.add(stmexploitation);

        jMenuBar1.add(jMenu6);

        jMenu8.setText("Sociétes");

        Societe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Societe.png"))); // NOI18N
        Societe.setText("Liste des Sociétes");
        Societe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SocieteActionPerformed(evt);
            }
        });
        jMenu8.add(Societe);
        jMenu8.add(jSeparator2);

        Depart.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/department.png"))); // NOI18N
        Depart.setText("Départements des Sociétes");
        Depart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepartActionPerformed(evt);
            }
        });
        jMenu8.add(Depart);
        jMenu8.add(jSeparator7);

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/bloggif_56efb792a8e14.png"))); // NOI18N
        jMenuItem5.setText("Site (Lieu)");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu8.add(jMenuItem5);

        jMenuBar1.add(jMenu8);

        jMenu2.setText("Etat");

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Demandes-d-Actes-d-Etat-Civil_large.jpg"))); // NOI18N
        jMenuItem3.setText("Etat du Parc");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Maintenance");

        sauv.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/sauver.png"))); // NOI18N
        sauv.setText("Exportation");
        sauv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sauvActionPerformed(evt);
            }
        });
        jMenu3.add(sauv);
        jMenu3.add(jSeparator10);

        restauration.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/importer.png"))); // NOI18N
        restauration.setText("Restauration");
        restauration.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restaurationActionPerformed(evt);
            }
        });
        jMenu3.add(restauration);

        jMenuBar1.add(jMenu3);

        jMenu7.setText("Autre");

        marque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/marque.png"))); // NOI18N
        marque.setText("Marques");
        marque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                marqueActionPerformed(evt);
            }
        });
        jMenu7.add(marque);
        jMenu7.add(jSeparator8);

        fournisseur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Fourn.png"))); // NOI18N
        fournisseur.setText("Fournisseurs");
        fournisseur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fournisseurActionPerformed(evt);
            }
        });
        jMenu7.add(fournisseur);
        jMenu7.add(jSeparator11);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/customize-512.png"))); // NOI18N
        jMenuItem4.setText("Liste des Intervenants");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem4);

        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void DeconnecterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DeconnecterActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment deconnecter?", "Deconnexion",
                JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
            dispose();
            Login l = new Login();
            l.setVisible(true);
        }
    }//GEN-LAST:event_DeconnecterActionPerformed

    private void quitterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitterActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment quitter?", "Fermeture",
                JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_quitterActionPerformed

    private void ListeUtilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ListeUtilActionPerformed
        try {
            Utilisateur l = new Utilisateur(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ListeUtilActionPerformed

    private void SocieteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SocieteActionPerformed
        try {
            Societe l = new Societe(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_SocieteActionPerformed

    private void DepartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepartActionPerformed
        try {
            Departement l = new Departement(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_DepartActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        ListeUser l = new ListeUser(new javax.swing.JFrame(), true);
        l.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void lstordinateurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lstordinateurActionPerformed

        try {
            ListeMachine l = new ListeMachine(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_lstordinateurActionPerformed

    private void lstecranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lstecranActionPerformed
        try {
            Ecran l = new Ecran(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_lstecranActionPerformed

    private void marqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_marqueActionPerformed
        try {
            Marque l = new Marque(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_marqueActionPerformed

    private void fournisseurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fournisseurActionPerformed
        try {
            Fournisseur l = new Fournisseur(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_fournisseurActionPerformed

    private void lstlogicielActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lstlogicielActionPerformed
        try {
            Logiciel l = new Logiciel(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_lstlogicielActionPerformed

    private void stmexploitationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stmexploitationActionPerformed
        try {
            Systeme l = new Systeme(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_stmexploitationActionPerformed

    private void sauvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sauvActionPerformed
        Sauvegarder l = new Sauvegarder(new javax.swing.JFrame(), true);
        l.setVisible(true);
    }//GEN-LAST:event_sauvActionPerformed

    private void restaurationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restaurationActionPerformed
        Sauvegarder l = new Sauvegarder(new javax.swing.JFrame(), true);
        l.setVisible(true);
    }//GEN-LAST:event_restaurationActionPerformed

    private void lstmaterielActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lstmaterielActionPerformed
        try {
            Materiel l = new Materiel(new javax.swing.JFrame(), true);
            l.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_lstmaterielActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        try {
            etat e = new etat(new javax.swing.JFrame(), true);
            e.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        try {
            Reparateur e = new Reparateur(new javax.swing.JFrame(), true);
            e.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
                try {
            Lieu e = new Lieu(new javax.swing.JFrame(), true);
            e.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(PrincipaleAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            // java.util.logging.Logger.getLogger(PrincipaleUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            //java.util.logging.Logger.getLogger(PrincipaleUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            // java.util.logging.Logger.getLogger(PrincipaleUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            // java.util.logging.Logger.getLogger(PrincipaleUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PrincipaleAdmin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Deconnecter;
    private javax.swing.JMenuItem Depart;
    private javax.swing.JMenuItem ListeUtil;
    private javax.swing.JMenuItem Societe;
    private javax.swing.JMenuItem fournisseur;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JMenuItem lstecran;
    private javax.swing.JMenuItem lstlogiciel;
    private javax.swing.JMenuItem lstmateriel;
    private javax.swing.JMenuItem lstordinateur;
    private javax.swing.JMenuItem marque;
    private javax.swing.JMenuItem quitter;
    private javax.swing.JMenuItem restauration;
    private javax.swing.JMenuItem sauv;
    private javax.swing.JMenuItem stmexploitation;
    // End of variables declaration//GEN-END:variables
 private static class ecouteur implements WindowListener {

        public ecouteur() {
        }

        @Override
        public void windowOpened(WindowEvent we) {
        }

        @Override
        public void windowClosing(WindowEvent we) {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment quitter le logiciel?", "Confirmation de fermeture",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                System.exit(0);
            }
        }

        @Override
        public void windowClosed(WindowEvent we) {

        }

        @Override
        public void windowIconified(WindowEvent we) {

        }

        @Override
        public void windowDeiconified(WindowEvent we) {

        }

        @Override
        public void windowActivated(WindowEvent we) {

        }

        @Override
        public void windowDeactivated(WindowEvent we) {

        }
    }
}
