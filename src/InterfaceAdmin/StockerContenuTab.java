
package InterfaceAdmin;

import javax.swing.JTable;

public class StockerContenuTab {

    static String contenuTab[];

    public void stocker(JTable tab) {
        int nbrCol = tab.getColumnCount();
        int nbrLigne = tab.getSelectedRow();

        contenuTab = new String[nbrCol];

        for (int i = 0; i < tab.getColumnCount(); i++) {
            if (tab.getValueAt(nbrLigne, i) == null) {
                contenuTab[i] = "";
            } else {
                contenuTab[i] = tab.getValueAt(nbrLigne, i).toString();
            }
        }
    }

}
