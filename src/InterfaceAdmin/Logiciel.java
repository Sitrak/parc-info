package InterfaceAdmin;

import ConnectionBDD.Connecter;
import static InterfaceAdmin.LogicielFormulaire.ValiderModifier;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class Logiciel extends javax.swing.JDialog {

    Connecter conn = new Connecter();

    ResultSet resultat,rs;
    Statement stmt;
    DefaultTableModel model = new DefaultTableModel();
    
    public Logiciel(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();

        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
            
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));

        modifier.setEnabled(false);
        Supprimer.setEnabled(false);

        //Ajout des colonnes liste
        model.addColumn("Id");
        model.addColumn("Type");
        model.addColumn("Nom du Logiciel");
        model.addColumn("Nom marque");
        model.addColumn("Date achat");
        model.addColumn("N° de serie");
        model.addColumn("Prix d'achat");
        model.addColumn("Societe");
        model.addColumn("Description");

        //Afficher tous le machine au demarrage
        resultat = stmt.executeQuery("Select * from logiciel");
        Actualiser(resultat);
    }

    public void Actualiser(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    resultat.getString("id_Log"),
                    resultat.getString("type_Log"),
                    resultat.getString("nom_log"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("date_Achat"),
                    resultat.getString("serial_log"),
                    resultat.getString("prix_Achat"),
                    resultat.getString("nom_St"),
                    resultat.getString("description_Log")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tabLog.setModel(model);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtGp = new javax.swing.ButtonGroup();
        Type = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        Tableau = new javax.swing.JPanel();
        Liste = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabLog = new javax.swing.JTable();
        Recherche = new javax.swing.JPanel();
        txtRecherche = new javax.swing.JTextField();
        Chercher = new javax.swing.JButton();
        ToutMachine = new javax.swing.JRadioButton();
        Station = new javax.swing.JRadioButton();
        MachineEnStock = new javax.swing.JRadioButton();
        jPanel4 = new javax.swing.JPanel();
        Fermer = new javax.swing.JButton();
        Supprimer = new javax.swing.JButton();
        exporter = new javax.swing.JButton();
        Nouveau1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        modifier = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Listes des logiciels - Administrateur");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        Liste.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Liste des Logiciels", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        tabLog.setAutoCreateRowSorter(true);
        tabLog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabLog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabLogMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tabLog);

        javax.swing.GroupLayout ListeLayout = new javax.swing.GroupLayout(Liste);
        Liste.setLayout(ListeLayout);
        ListeLayout.setHorizontalGroup(
            ListeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
        );
        ListeLayout.setVerticalGroup(
            ListeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
        );

        txtRecherche.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtRecherche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRechercheActionPerformed(evt);
            }
        });
        txtRecherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRechercheKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRechercheKeyTyped(evt);
            }
        });

        Chercher.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Chercher.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/recherher.png"))); // NOI18N
        Chercher.setText("Rechercher");
        Chercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChercherActionPerformed(evt);
            }
        });
        Chercher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ChercherKeyPressed(evt);
            }
        });

        buttonGroup1.add(ToutMachine);
        ToutMachine.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ToutMachine.setSelected(true);
        ToutMachine.setText("Tous");
        ToutMachine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ToutMachineActionPerformed(evt);
            }
        });

        buttonGroup1.add(Station);
        Station.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Station.setText("Gratuit");
        Station.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StationActionPerformed(evt);
            }
        });

        buttonGroup1.add(MachineEnStock);
        MachineEnStock.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        MachineEnStock.setText("Payant");
        MachineEnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MachineEnStockActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout RechercheLayout = new javax.swing.GroupLayout(Recherche);
        Recherche.setLayout(RechercheLayout);
        RechercheLayout.setHorizontalGroup(
            RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RechercheLayout.createSequentialGroup()
                .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Chercher)
                .addGap(18, 18, 18)
                .addComponent(ToutMachine)
                .addGap(118, 118, 118)
                .addComponent(Station)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 128, Short.MAX_VALUE)
                .addComponent(MachineEnStock)
                .addGap(103, 103, 103))
        );
        RechercheLayout.setVerticalGroup(
            RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RechercheLayout.createSequentialGroup()
                .addGroup(RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(MachineEnStock)
                        .addComponent(ToutMachine)
                        .addComponent(Station))
                    .addGroup(RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Chercher)))
                .addGap(0, 2, Short.MAX_VALUE))
        );

        Fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        Fermer.setText("Fermer");
        Fermer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FermerActionPerformed(evt);
            }
        });

        Supprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Supprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Supprimer.png"))); // NOI18N
        Supprimer.setText("Supprimer");
        Supprimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Supprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SupprimerActionPerformed(evt);
            }
        });

        exporter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        exporter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/export.png"))); // NOI18N
        exporter.setText("Exporter");
        exporter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        exporter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exporterActionPerformed(evt);
            }
        });

        Nouveau1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Nouveau1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Imprimer.png"))); // NOI18N
        Nouveau1.setText("Imprimer");
        Nouveau1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Nouveau1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nouveau1ActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Nouveau.png"))); // NOI18N
        jButton1.setText("Nouveau");
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        modifier.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Supprimer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(modifier, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Nouveau1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(exporter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Fermer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator2))
                .addGap(1, 1, 1))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jButton1)
                .addGap(6, 6, 6)
                .addComponent(Supprimer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(modifier)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Nouveau1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exporter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Fermer, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout TableauLayout = new javax.swing.GroupLayout(Tableau);
        Tableau.setLayout(TableauLayout);
        TableauLayout.setHorizontalGroup(
            TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableauLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TableauLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(Recherche, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(Liste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        TableauLayout.setVerticalGroup(
            TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableauLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(TableauLayout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(TableauLayout.createSequentialGroup()
                        .addComponent(Recherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Liste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tableau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Tableau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //Supression de ligne
    private void SupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SupprimerActionPerformed
            String selectionner = model.getValueAt(tabLog.getSelectedRow(), 0).toString();
            try {
                if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment supprimer cette logiciel ?", "Confirmation Suppression",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                    model.removeRow(tabLog.getSelectedRow());
                    stmt.executeUpdate("DELETE FROM logiciel WHERE  id_Log = '" + selectionner + "'");

                    resultat = stmt.executeQuery("Select * from logiciel");
                    Actualiser(resultat);
                    modifier.setEnabled(false);
                    Supprimer.setEnabled(false);
                }
            } catch (HeadlessException | SQLException e) {
                System.err.println(e);
                JOptionPane.showMessageDialog(null, e);
            }
        
    }//GEN-LAST:event_SupprimerActionPerformed

    private void FermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FermerActionPerformed
        dispose();
    }//GEN-LAST:event_FermerActionPerformed

    private void tabLogMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabLogMouseClicked
        modifier.setEnabled(true);
        Supprimer.setEnabled(true);
    }//GEN-LAST:event_tabLogMouseClicked

    private void Nouveau1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nouveau1ActionPerformed
        MessageFormat header = new MessageFormat("Liste des logiciels");
        MessageFormat footer = new MessageFormat("");
        try {
            tabLog.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } catch (PrinterException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Nouveau1ActionPerformed

    private void ChercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChercherActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from logiciel where "
                    + "id_Log = '" + txtRecherche.getText() + "'"
                    + "or type_Log = '" + txtRecherche.getText() + "'"
                    + "or nom_log = '" + txtRecherche.getText() + "'"
                    + "or nom_Marque = '" + txtRecherche.getText() + "'"
                    + "or date_Achat = '" + txtRecherche.getText() + "'"
                    + "or serial_log = '" + txtRecherche.getText() + "'"
                    + "or prix_Achat = '" + txtRecherche.getText() + "'"
                    + "or nom_St = '" + txtRecherche.getText() + "'"
                    + "or description_Log = '" + txtRecherche.getText() + "'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_ChercherActionPerformed

    private void txtRechercheKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyPressed

    }//GEN-LAST:event_txtRechercheKeyPressed

    private void txtRechercheKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyTyped


    }//GEN-LAST:event_txtRechercheKeyTyped

    private void ChercherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ChercherKeyPressed

    }//GEN-LAST:event_ChercherKeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void exporterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exporterActionPerformed
        ExporterExcel.exporter(this, tabLog);
    }//GEN-LAST:event_exporterActionPerformed

    private void StationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StationActionPerformed
        try {

            resultat = stmt.executeQuery("Select * from logiciel where type_Log = 'Gratuit'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_StationActionPerformed

    private void MachineEnStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MachineEnStockActionPerformed

        try {

            resultat = stmt.executeQuery("Select * from logiciel where type_Log = 'Payant'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MachineEnStockActionPerformed

    private void ToutMachineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ToutMachineActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from logiciel");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_ToutMachineActionPerformed

    private void txtRechercheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRechercheActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRechercheActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        LogicielFormulaire l = new LogicielFormulaire(new javax.swing.JFrame(), true);
        ValiderModifier.setEnabled(false);
        l.setVisible(true);
                //Actualiser le tableau si le fenetre ferme
        if (l.isShowing() == false) {
            try {
                rs = stmt.executeQuery("Select * from logiciel");
                Actualiser(rs);
                modifier.setEnabled(false);
                Supprimer.setEnabled(false);
            } catch (SQLException ex) {
                Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        LogicielFormulaire l = new LogicielFormulaire(new javax.swing.JFrame(), true);
        l.AfficheContenu();
        l.setVisible(true);
        //Actualiser le tableau si le fenetre ferme
        if (l.isShowing() == false) {
            try {
                rs = stmt.executeQuery("Select * from logiciel");
                Actualiser(rs);
                modifier.setEnabled(false);
                Supprimer.setEnabled(false);
            } catch (SQLException ex) {
                Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_modifierActionPerformed

    public static void main(String args[]) throws SQLException {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Logiciel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Logiciel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Logiciel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Logiciel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }


        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Logiciel dialog = new Logiciel(new javax.swing.JFrame(), true);

                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup BtGp;
    private javax.swing.JButton Chercher;
    private javax.swing.JButton Fermer;
    private javax.swing.JPanel Liste;
    private javax.swing.JRadioButton MachineEnStock;
    private javax.swing.JButton Nouveau1;
    private javax.swing.JPanel Recherche;
    private javax.swing.JRadioButton Station;
    public static javax.swing.JButton Supprimer;
    private javax.swing.JPanel Tableau;
    private javax.swing.JRadioButton ToutMachine;
    private javax.swing.ButtonGroup Type;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton exporter;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public static javax.swing.JButton modifier;
    public static javax.swing.JTable tabLog;
    private javax.swing.JTextField txtRecherche;
    // End of variables declaration//GEN-END:variables
}
