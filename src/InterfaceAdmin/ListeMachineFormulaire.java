package InterfaceAdmin;

import ConnectionBDD.Connecter;
import static InterfaceAdmin.ListeMachine.Modifier;
import static InterfaceAdmin.ListeMachine.Supprimer;
import static InterfaceAdmin.ListeMachine.tab;
import static InterfaceAdmin.StockerContenuTab.contenuTab;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class ListeMachineFormulaire extends javax.swing.JDialog {

    Connecter conn = new Connecter();

    ResultSet resultat, rs;
    Statement stmt;

    public ListeMachineFormulaire(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));

        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        
        RemplirCB("nom_Site", "site", site);
        RemplirCB("cpu", "cpu", cpu);
        RemplirCB("ram", "ram", ram);
        RemplirCB("hdd", "hdd", hdd);
        RemplirCB("nom_Util", "utilisateur", affecte);
        RemplirCB("nom_St", "societe", ste);
        RemplirCB("nom_Marque", "marque", marque);
        RemplirCB("nom_Fournisseur", "fournisseur", fournisseurCB);
        RemplirCB("nom_Syst", "systeme", SE);
        RemplirCB("nom_Ecran", "ecran", ecran);
        
    }

    public void ViderLeChamp() {
        //Vider le champ   
        situation.setSelectedItem("");
        type_Machine.setSelectedItem("");
        nomMachine.setText("");
        dateAc.setText("");
        adIp.setText("");
        modele.setText("");
        durreGar.setText("");
        prixAc.setText("");
        autre.setText("");
        
        RemplirCB("nom_Site", "site", site);
        RemplirCB("cpu", "cpu", cpu);
        RemplirCB("ram", "ram", ram);
        RemplirCB("hdd", "hdd", hdd);
        RemplirCB("nom_Util", "utilisateur", affecte);
        RemplirCB("nom_St", "societe", ste);
        RemplirCB("nom_Marque", "marque", marque);
        RemplirCB("nom_Fournisseur", "fournisseur", fournisseurCB);
        RemplirCB("nom_Syst", "systeme", SE);
        RemplirCB("nom_Ecran", "ecran", ecran);
    }
    
    public void AfficheContenu() {
        StockerContenuTab s = new StockerContenuTab();
        s.stocker(tab);
            situation.setSelectedItem(contenuTab[1]);
            type_Machine.setSelectedItem(contenuTab[2]);
            nomMachine.setText(contenuTab[3]);
            marque.setSelectedItem(contenuTab[4]);
            affecte.setSelectedItem(contenuTab[5]);
            site.setSelectedItem(contenuTab[6]);
            ste.setSelectedItem(contenuTab[7]);
            adIp.setText(contenuTab[8]);
            fournisseurCB.setSelectedItem(contenuTab[9]);
            dateAc.setText(contenuTab[10]);
            durreGar.setText(contenuTab[11]);
            prixAc.setText(contenuTab[12]);
            modele.setText(contenuTab[13]);
            SE.setSelectedItem(contenuTab[14]);
            cpu.setSelectedItem(contenuTab[15]);
            ram.setSelectedItem(contenuTab[16]);
            hdd.setSelectedItem(contenuTab[17]);
            ecran.setSelectedItem(contenuTab[18]);
            autre.setText(contenuTab[19]);
    }

    public void RemplirCB(String primaire, String table, JComboBox CB) {
        String req = "SELECT "+ primaire +" FROM  " + table + "";

        CB.removeAllItems();
        CB.addItem("");
        try {
            resultat = stmt.executeQuery(req);
            while (resultat.next()) {
                CB.addItem(resultat.getString(1));
            }
            resultat.close();
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Caracteristique = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        ecran = new javax.swing.JComboBox<>();
        BrEcran = new javax.swing.JButton();
        jLabel44 = new javax.swing.JLabel();
        adIp = new javax.swing.JFormattedTextField();
        jLabel45 = new javax.swing.JLabel();
        marque = new javax.swing.JComboBox<>();
        BtMarque = new javax.swing.JButton();
        jLabel46 = new javax.swing.JLabel();
        modele = new javax.swing.JTextField();
        jLabel47 = new javax.swing.JLabel();
        SE = new javax.swing.JComboBox<>();
        BtSyst = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        cpu = new javax.swing.JComboBox<>();
        ram = new javax.swing.JComboBox<>();
        jButton4 = new javax.swing.JButton();
        hdd = new javax.swing.JComboBox<>();
        jButton5 = new javax.swing.JButton();
        jLabel32 = new javax.swing.JLabel();
        type_Machine = new javax.swing.JComboBox<>();
        Formulaire1 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        situation = new javax.swing.JComboBox<>();
        jLabel33 = new javax.swing.JLabel();
        affecte = new javax.swing.JComboBox<>();
        jLabel34 = new javax.swing.JLabel();
        fournisseurCB = new javax.swing.JComboBox<>();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        durreGar = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        prixAc = new javax.swing.JTextField();
        jLabel38 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        autre = new javax.swing.JTextArea();
        jLabel39 = new javax.swing.JLabel();
        nomMachine = new javax.swing.JTextField();
        Utilisateur = new javax.swing.JButton();
        frns = new javax.swing.JButton();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        site = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        dateAc = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        ste = new javax.swing.JComboBox<>();
        ValiderModification = new javax.swing.JToggleButton();
        ValiderCommeNouveau = new javax.swing.JToggleButton();
        Annuler = new javax.swing.JButton();
        vider = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Formulaire de la liste des ordinateurs");

        Caracteristique.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Caracteristique", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        jLabel40.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel40.setText("RAM");

        jLabel41.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel41.setText("HDD");

        jLabel42.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel42.setText("CPU");

        jLabel43.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel43.setText("Ecran");

        ecran.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ecran.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ecran.setSelectedIndex(-1);
        ecran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ecranActionPerformed(evt);
            }
        });

        BrEcran.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        BrEcran.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/lcd_monitor.png"))); // NOI18N
        BrEcran.setText("Ecran");
        BrEcran.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BrEcranActionPerformed(evt);
            }
        });

        jLabel44.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel44.setText("Adresse Ip");

        try {
            adIp.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###.###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        adIp.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        adIp.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        adIp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adIpActionPerformed(evt);
            }
        });

        jLabel45.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel45.setText("Marque");

        marque.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        marque.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        marque.setSelectedIndex(-1);

        BtMarque.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        BtMarque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/marque.png"))); // NOI18N
        BtMarque.setText("Marque");
        BtMarque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtMarqueActionPerformed(evt);
            }
        });

        jLabel46.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel46.setText("Modèle du carte mère");

        modele.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        modele.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modeleActionPerformed(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel47.setText("Systèm d'exploitation");

        SE.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        SE.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        SE.setSelectedIndex(-1);

        BtSyst.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        BtSyst.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/téléchargement-(4).png"))); // NOI18N
        BtSyst.setText("Systeme");
        BtSyst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtSystActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/bloggif_56efb7859520c.png"))); // NOI18N
        jButton3.setText("Processeur");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        cpu.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        ram.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jButton4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/bloggif_56efb79fe0b06.png"))); // NOI18N
        jButton4.setText("Ram");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        hdd.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jButton5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cpu.png"))); // NOI18N
        jButton5.setText("HDD");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel32.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel32.setText("Type");

        type_Machine.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        type_Machine.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Station", "Portable" }));
        type_Machine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                type_MachineActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout CaracteristiqueLayout = new javax.swing.GroupLayout(Caracteristique);
        Caracteristique.setLayout(CaracteristiqueLayout);
        CaracteristiqueLayout.setHorizontalGroup(
            CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CaracteristiqueLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CaracteristiqueLayout.createSequentialGroup()
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel44)
                            .addComponent(jLabel42)
                            .addComponent(jLabel40)
                            .addComponent(jLabel43)
                            .addComponent(jLabel41))
                        .addGap(75, 75, 75)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(CaracteristiqueLayout.createSequentialGroup()
                                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ecran, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cpu, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(ram, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(hdd, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BrEcran, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(adIp)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CaracteristiqueLayout.createSequentialGroup()
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel47)
                            .addComponent(jLabel45)
                            .addComponent(jLabel32))
                        .addGap(9, 9, 9)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(type_Machine, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(CaracteristiqueLayout.createSequentialGroup()
                                .addGap(208, 208, 208)
                                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(BtMarque, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(BtSyst, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(CaracteristiqueLayout.createSequentialGroup()
                        .addComponent(jLabel46)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(modele)
                            .addGroup(CaracteristiqueLayout.createSequentialGroup()
                                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(marque, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(SE, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        CaracteristiqueLayout.setVerticalGroup(
            CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, CaracteristiqueLayout.createSequentialGroup()
                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(type_Machine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(marque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtMarque)
                    .addComponent(jLabel45))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtSyst)
                    .addComponent(SE, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(modele, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel46))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CaracteristiqueLayout.createSequentialGroup()
                        .addComponent(adIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ecran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BrEcran))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cpu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel42))
                            .addComponent(jButton3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ram, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton4)
                            .addComponent(jLabel40))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(CaracteristiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(hdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton5)
                            .addComponent(jLabel41)))
                    .addGroup(CaracteristiqueLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel44)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel43)))
                .addContainerGap())
        );

        Formulaire1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Géneralité", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        jLabel31.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel31.setText("Situation");

        situation.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        situation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "En service", "En stock" }));
        situation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                situationActionPerformed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel33.setText("Affecté à");

        affecte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        affecte.setToolTipText("");

        jLabel34.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel34.setText("Fournisseur");

        fournisseurCB.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        fournisseurCB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        fournisseurCB.setSelectedIndex(-1);

        jLabel35.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel35.setText("Date d'achat");

        jLabel36.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel36.setText("Durée de Garantie (En mois)");

        durreGar.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        durreGar.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel37.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel37.setText("Prix d'achat");

        prixAc.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        prixAc.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel38.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel38.setText("Autre description");

        autre.setColumns(20);
        autre.setRows(5);
        jScrollPane5.setViewportView(autre);

        jLabel39.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel39.setText("Nom du machine");

        nomMachine.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        nomMachine.setToolTipText("");
        nomMachine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomMachineActionPerformed(evt);
            }
        });

        Utilisateur.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Utilisateur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Utilisateur.png"))); // NOI18N
        Utilisateur.setText("Utilisateur");
        Utilisateur.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Utilisateur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UtilisateurActionPerformed(evt);
            }
        });

        frns.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        frns.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Fourn.png"))); // NOI18N
        frns.setText("Fournisseur");
        frns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                frnsActionPerformed(evt);
            }
        });

        jLabel52.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel52.setText("Lieu");

        jLabel53.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel53.setText("Société");

        site.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jButton1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Societe.png"))); // NOI18N
        jButton1.setText("Société");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        dateAc.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        dateAc.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        dateAc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateAcMouseClicked(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/bloggif_56efb792a8e14.png"))); // NOI18N
        jButton2.setText("Lieu");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        ste.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        javax.swing.GroupLayout Formulaire1Layout = new javax.swing.GroupLayout(Formulaire1);
        Formulaire1.setLayout(Formulaire1Layout);
        Formulaire1Layout.setHorizontalGroup(
            Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Formulaire1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Formulaire1Layout.createSequentialGroup()
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel36)
                            .addComponent(jLabel37)
                            .addComponent(jLabel38))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(prixAc)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(durreGar, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(Formulaire1Layout.createSequentialGroup()
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel31)
                            .addComponent(jLabel39)
                            .addComponent(jLabel33)
                            .addComponent(jLabel35))
                        .addGap(70, 70, 70)
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nomMachine, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(Formulaire1Layout.createSequentialGroup()
                                .addComponent(affecte, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(Utilisateur, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(dateAc)
                            .addComponent(situation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(Formulaire1Layout.createSequentialGroup()
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel52)
                            .addComponent(jLabel53)
                            .addComponent(jLabel34))
                        .addGap(104, 104, 104)
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(fournisseurCB, 0, 199, Short.MAX_VALUE)
                            .addComponent(ste, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(site, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(Formulaire1Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Formulaire1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(frns, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        Formulaire1Layout.setVerticalGroup(
            Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Formulaire1Layout.createSequentialGroup()
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(situation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nomMachine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel39))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(affecte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel33)
                    .addComponent(Utilisateur))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel52)
                    .addComponent(jButton2)
                    .addComponent(site, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel53))
                    .addComponent(ste, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(fournisseurCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel34))
                    .addComponent(frns))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(dateAc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(durreGar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prixAc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel37))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Formulaire1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel38)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        ValiderModification.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ValiderModification.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        ValiderModification.setText("Modifier");
        ValiderModification.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderModificationActionPerformed(evt);
            }
        });

        ValiderCommeNouveau.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ValiderCommeNouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Ajouter.png"))); // NOI18N
        ValiderCommeNouveau.setText("Ajouter");
        ValiderCommeNouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ValiderCommeNouveauActionPerformed(evt);
            }
        });

        Annuler.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Annuler.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        Annuler.setText("Fermer");
        Annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnnulerActionPerformed(evt);
            }
        });

        vider.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        vider.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Vider.png"))); // NOI18N
        vider.setText("Vider les champs");
        vider.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        vider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 530, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(Caracteristique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ValiderCommeNouveau)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ValiderModification)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vider)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Annuler, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(13, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(Formulaire1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(507, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Caracteristique, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ValiderCommeNouveau)
                    .addComponent(ValiderModification)
                    .addComponent(vider)
                    .addComponent(Annuler))
                .addContainerGap(19, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(Formulaire1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void situationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_situationActionPerformed

        if (situation.getSelectedItem().equals("En stock")) {
            affecte.setEnabled(false);
            site.setEnabled(false);
            adIp.setEnabled(false);
            ecran.setEnabled(false);
            affecte.setSelectedItem("");
            site.setSelectedItem("");
            ecran.setSelectedItem("");
            adIp.setText("");
        } else {
            affecte.setEnabled(true);
            site.setEnabled(true);
            adIp.setEnabled(true);
            ecran.setEnabled(true);
        }
    }//GEN-LAST:event_situationActionPerformed

    private void type_MachineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_type_MachineActionPerformed
        if (type_Machine.getSelectedItem().equals("Portable")) {
            ecran.setEnabled(false);
        } else {
            ecran.setEnabled(true);
        }
    }//GEN-LAST:event_type_MachineActionPerformed

    private void nomMachineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomMachineActionPerformed

    }//GEN-LAST:event_nomMachineActionPerformed

    private void UtilisateurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UtilisateurActionPerformed
        try {
            Utilisateur u = new Utilisateur(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_Util", "utilisateur", affecte);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_UtilisateurActionPerformed

    private void frnsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_frnsActionPerformed
        try {
            Fournisseur i = new Fournisseur(new javax.swing.JFrame(), true);
            i.setVisible(true);
            if (i.isShowing() == false) {
                RemplirCB("nom_Fournisseur", "fournisseur", fournisseurCB);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_frnsActionPerformed

    private void ecranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ecranActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ecranActionPerformed

    private void BrEcranActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BrEcranActionPerformed
        try {
            Ecran u = new Ecran(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_Ecran", "ecran", ecran);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BrEcranActionPerformed

    private void adIpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_adIpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_adIpActionPerformed

    private void BtMarqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtMarqueActionPerformed
        try {
            Marque u = new Marque(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_Marque", "marque", marque);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtMarqueActionPerformed

    private void modeleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modeleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_modeleActionPerformed

    private void BtSystActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtSystActionPerformed
        try {
            Systeme u = new Systeme(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_Syst", "systeme", SE);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_BtSystActionPerformed

    private void ValiderCommeNouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ValiderCommeNouveauActionPerformed
        String requete
                = "insert into machine"
                + "(situation,type,nom_util,lieu,nom_St,nom_Machine,adresse_IP,date_Achat,duree_Garantie,prix_Machine,modele_Machine,cpu,ram,ecran,hdd,autre_Description,nom_Marque,nom_Fournisseur,nom_Syst)VALUES('"
                + situation.getSelectedItem().toString() + "','"
                + type_Machine.getSelectedItem().toString() + "','"
                + affecte.getSelectedItem().toString() + "','"
                + site.getSelectedItem().toString() + "','"
                + ste.getSelectedItem() + "','"
                + nomMachine.getText() + "','"
                + adIp.getText() + "','"
                + dateAc.getText() + "','"
                + durreGar.getText() + "','"
                + prixAc.getText() + "','"
                + modele.getText() + "','"
                + cpu.getSelectedItem().toString() + "','"
                + ram.getSelectedItem().toString() + "','"
                + ecran.getSelectedItem().toString() + "','"
                + hdd.getSelectedItem().toString() + "','"
                + autre.getText() + "','"
                + marque.getSelectedItem().toString() + "','"
                + fournisseurCB.getSelectedItem().toString() + "','"
                + SE.getSelectedItem().toString() + "')";

        if (situation.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner un situation");
        }  else if (affecte.getSelectedItem().equals("") && situation.getSelectedItem().equals("En service")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner une utilisateur");
        } else if (site.getSelectedItem().toString().equals("") && situation.getSelectedItem().equals("En service")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner le lieu");
        } else if (ste.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner une societe");
        } else if (dateAc.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Date d'achat est obligatoire");
        }else if (type_Machine.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner le type de l'ordinateur");
        } else if (marque.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner une marque");
        } else if (SE.getSelectedItem().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner une systeme d'exploitation");
        } else if (ecran.getSelectedItem().equals("") && situation.getSelectedItem().equals("En service")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner une ecran");
        } else if (cpu.getSelectedItem().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner une type Processeur");
        } else if (ram.getSelectedItem().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner la Ram");
        } else if (hdd.getSelectedItem().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionner le Disque dur");
        } else {
            try {
                stmt.executeUpdate(requete);  
                dispose();
                JOptionPane.showMessageDialog(null, "Ajout effectué");                
            } catch (SQLException ex) {
                System.err.println(ex);
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }//GEN-LAST:event_ValiderCommeNouveauActionPerformed

    private void ValiderModificationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ValiderModificationActionPerformed
        String selectionner = tab.getValueAt(tab.getSelectedRow(), 0).toString();

        String update = "update  machine set "
                + "situation = '" + situation.getSelectedItem().toString() + "',"
                + "type = '" + type_Machine.getSelectedItem().toString() + "',"
                + "nom_util = '" + affecte.getSelectedItem().toString() + "',"
                + "lieu = '" + site.getSelectedItem().toString() + "',"
                + "nom_St = '" + ste.getSelectedItem().toString() + "',"
                + "nom_Machine = '" + nomMachine.getText() + "',"
                + "adresse_IP = '" + adIp.getText() + "',"
                + "date_Achat = '" + dateAc.getText() + "',"
                + "duree_Garantie = '" + durreGar.getText() + "',"
                + "prix_Machine = '" + prixAc.getText() + "',"
                + "modele_Machine =' " + modele.getText() + "',"
                + "cpu = '" + cpu.getSelectedItem().toString() + "',"
                + "ram = '" + ram.getSelectedItem().toString() + "',"
                + "ecran = '" + ecran.getSelectedItem().toString() + "',"
                + "hdd = '" + hdd.getSelectedItem().toString() + "',"
                + "autre_Description = '" + autre.getText() + "',"
                + "nom_Syst = '" + SE.getSelectedItem().toString() + "',"
                + "nom_Marque = '" + marque.getSelectedItem().toString() + "',"
                + "nom_Fournisseur = '" + fournisseurCB.getSelectedItem().toString() + "' where id_Machine = '" + selectionner + "' ";

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez modifier cette machine ?", "Confirmation de modification",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                stmt.executeUpdate(update);
                dispose();
                JOptionPane.showMessageDialog(null, "Modification terminé");                
            }
        } catch (SQLException ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_ValiderModificationActionPerformed

    private void AnnulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnnulerActionPerformed
        dispose();
    }//GEN-LAST:event_AnnulerActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            Societe u = new Societe(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_St", "societe", ste);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void dateAcMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateAcMouseClicked
        Calendrier i = new Calendrier(new javax.swing.JFrame(), true);
        i.setVisible(true);
        if (i.isShowing() == false) {
            i.dateTxt(dateAc);
        }
    }//GEN-LAST:event_dateAcMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            Lieu u = new Lieu(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("nom_Site", "site", site);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            Cpu u = new Cpu(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("cpu", "cpu", cpu);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try {
            Ram u = new Ram(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("ram", "ram", ram);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        try {
            Hdd u = new Hdd(new javax.swing.JFrame(), true);
            u.setVisible(true);
            if (u.isShowing() == false) {
                RemplirCB("hdd", "hdd", hdd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void viderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viderActionPerformed
        ViderLeChamp();
        //Desactiver les bouton
        ValiderModification.setEnabled(false);
    }//GEN-LAST:event_viderActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListeMachineFormulaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListeMachineFormulaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListeMachineFormulaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListeMachineFormulaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ListeMachineFormulaire dialog = new ListeMachineFormulaire(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Annuler;
    private javax.swing.JButton BrEcran;
    private javax.swing.JButton BtMarque;
    private javax.swing.JButton BtSyst;
    private javax.swing.JPanel Caracteristique;
    private javax.swing.JPanel Formulaire1;
    public static javax.swing.JComboBox<String> SE;
    private javax.swing.JButton Utilisateur;
    private javax.swing.JToggleButton ValiderCommeNouveau;
    public static javax.swing.JToggleButton ValiderModification;
    public static javax.swing.JFormattedTextField adIp;
    public static javax.swing.JComboBox<String> affecte;
    public static javax.swing.JTextArea autre;
    private javax.swing.JComboBox<String> cpu;
    public static javax.swing.JTextField dateAc;
    public static javax.swing.JTextField durreGar;
    public static javax.swing.JComboBox<String> ecran;
    public static javax.swing.JComboBox<String> fournisseurCB;
    private javax.swing.JButton frns;
    private javax.swing.JComboBox<String> hdd;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JScrollPane jScrollPane5;
    public static javax.swing.JComboBox<String> marque;
    public static javax.swing.JTextField modele;
    public static javax.swing.JTextField nomMachine;
    public static javax.swing.JTextField prixAc;
    private javax.swing.JComboBox<String> ram;
    public static javax.swing.JComboBox<String> site;
    private javax.swing.JComboBox<String> situation;
    public static javax.swing.JComboBox<String> ste;
    private javax.swing.JComboBox<String> type_Machine;
    private javax.swing.JButton vider;
    // End of variables declaration//GEN-END:variables
}
