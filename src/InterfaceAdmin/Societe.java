package InterfaceAdmin;

import ConnectionBDD.Connecter;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public final class Societe extends javax.swing.JDialog {

    Connecter conn = new Connecter();
    
    ResultSet resultat;
    Statement stmt;
    DefaultTableModel model = new DefaultTableModel();

    public Societe(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();

        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));
        
        //Desactiver les bouton
        modifier.setEnabled(false);
        vider.setEnabled(false);

        //Ajouter les colonnes
        model.addColumn("Nom societe");
        model.addColumn("Adresse");
        model.addColumn("Telephone");
        model.addColumn("Email");
        model.addColumn("Site web");
        model.addColumn("Description");

        //Afficher tous le machine au demarrage
        resultat = stmt.executeQuery("Select * from societe");
        Actualiser(resultat);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        nomSte = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        adresseSte = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        telSte = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        emailSte = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        siteSte = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        descriptionSte = new javax.swing.JTextArea();
        Ajouter = new javax.swing.JButton();
        modifier = new javax.swing.JButton();
        vider = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        supprimer = new javax.swing.JButton();
        Fermer = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Liste des Sociétés - Administrateur");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Nom");

        nomSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Adresse");

        adresseSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setText("Telephone");

        telSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setText("Email");

        emailSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setText("Site Web");

        siteSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setText("Description");

        descriptionSte.setColumns(20);
        descriptionSte.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        descriptionSte.setRows(5);
        jScrollPane1.setViewportView(descriptionSte);

        Ajouter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Ajouter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Ajouter.png"))); // NOI18N
        Ajouter.setText("Ajouter");
        Ajouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AjouterActionPerformed(evt);
            }
        });

        modifier.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });

        vider.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        vider.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Vider.png"))); // NOI18N
        vider.setText("Vider les champs");
        vider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viderActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nomSte)
                    .addComponent(adresseSte)
                    .addComponent(telSte)
                    .addComponent(emailSte)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Ajouter)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(modifier)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vider)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addComponent(siteSte))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nomSte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adresseSte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telSte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(emailSte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(siteSte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Ajouter)
                    .addComponent(modifier)
                    .addComponent(vider))
                .addContainerGap())
        );

        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tab);

        supprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        supprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Supprimer.png"))); // NOI18N
        supprimer.setText("Supprimer");
        supprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerActionPerformed(evt);
            }
        });

        Fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        Fermer.setText("Fermer");
        Fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FermerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(supprimer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(Fermer, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(supprimer)
                            .addComponent(Fermer))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    public void Actualiser(ResultSet resultat) {
        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    
                    resultat.getString("nom_St"),
                    resultat.getString("adresse_Ste"),
                    resultat.getString("tel_Ste"),
                    resultat.getString("email_Ste"),
                    resultat.getString("site_web"),
                    resultat.getString("description_Societe"),
                    resultat.getString("id_Ste")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tab.setModel(model);
    }

    private void ViderLeChamp() {
        //Vider le champ de test        
        nomSte.setText("");
        adresseSte.setText("");
        telSte.setText("");
        emailSte.setText("");
        siteSte.setText("");
        descriptionSte.setText("");
    }
       public void Deplacer(int i) {
        //Deplacer le contenu des lignes dans les champs de text
        try {
            nomSte.setText(model.getValueAt(i, 0).toString());
            adresseSte.setText(model.getValueAt(i, 1).toString());
            telSte.setText(model.getValueAt(i, 2).toString());
            emailSte.setText(model.getValueAt(i, 3).toString());
            siteSte.setText(model.getValueAt(i, 4).toString());
            descriptionSte.setText(model.getValueAt(i, 5).toString());
        } catch (Exception e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void AjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AjouterActionPerformed
        String nom = nomSte.getText();
        String adresse = adresseSte.getText();
        String tel = telSte.getText();
        String email = emailSte.getText();
        String site = siteSte.getText();
        String desc = descriptionSte.getText();
        String requete = "insert into societe(nom_St,adresse_Ste,tel_Ste,email_Ste,site_web,description_Societe)VALUES('"
                + nom + "','" + adresse + "','" + tel + "','" + email + "','" + site + "','" + desc + "')";
        if (nom.equals("")) {
            JOptionPane.showMessageDialog(null, "Veullez ramplir tous les champs");
        } else {
            try {
                stmt.executeUpdate(requete);
                JOptionPane.showMessageDialog(null, "La societe est bien ajoute");
                resultat = stmt.executeQuery("Select * from societe");
                Actualiser(resultat);
                ViderLeChamp();

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }//GEN-LAST:event_AjouterActionPerformed

    private void viderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viderActionPerformed
        ViderLeChamp();
        //Desactiver les bouton
        modifier.setEnabled(false);
        supprimer.setEnabled(false);        
    }//GEN-LAST:event_viderActionPerformed

    private void supprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerActionPerformed
        String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment supprimer cette societe ?", "Confirmation de suppression",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                model.removeRow(tab.getSelectedRow());
                stmt.executeUpdate("DELETE FROM societe WHERE  nom_St = '" + selectionner + "'");
                resultat = stmt.executeQuery("Select * from societe");
                Actualiser(resultat);
            }
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        ViderLeChamp();
    }//GEN-LAST:event_supprimerActionPerformed

    private void FermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FermerActionPerformed
            dispose();
    }//GEN-LAST:event_FermerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();

        String nom = nomSte.getText();
        String adresse = adresseSte.getText();
        String tel = telSte.getText();
        String email = emailSte.getText();
        String site = siteSte.getText();
        String desc = descriptionSte.getText();

        String update = "update  societe set "
                + "nom_St ='" + nom + "',"
                + "adresse_Ste= '" + adresse + "',"
                + "tel_Ste = '" + tel + "',"
                + "email_Ste = '" + email + "',"
                + "site_web = '" + site + "',"
                + "description_Societe = '" + desc + "'  where nom_St = '" + selectionner + "' ";

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez modifier cette societe ?", "Confirmation Modification",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                stmt.executeUpdate(update);
                resultat = stmt.executeQuery("Select * from societe");
                Actualiser(resultat);
                ViderLeChamp();
            }
        } catch (Exception ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }

    }//GEN-LAST:event_modifierActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        int i = tab.getSelectedRow();
        Deplacer(i);
        modifier.setEnabled(true);
        vider.setEnabled(true);
    }//GEN-LAST:event_tabMouseClicked

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Societe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Societe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Societe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Societe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Societe dialog = new Societe(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Societe.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ajouter;
    private javax.swing.JButton Fermer;
    private javax.swing.JTextField adresseSte;
    private javax.swing.JTextArea descriptionSte;
    private javax.swing.JTextField emailSte;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton modifier;
    private javax.swing.JTextField nomSte;
    private javax.swing.JTextField siteSte;
    private javax.swing.JButton supprimer;
    private javax.swing.JTable tab;
    private javax.swing.JTextField telSte;
    private javax.swing.JButton vider;
    // End of variables declaration//GEN-END:variables
}
