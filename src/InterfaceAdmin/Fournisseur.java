package InterfaceAdmin;

import ConnectionBDD.Connecter;
import java.awt.HeadlessException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import java.awt.Toolkit;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

public final class Fournisseur extends javax.swing.JDialog {

    Connecter conn = new Connecter();

    ResultSet resultat;
    Statement stmt;
    DefaultTableModel model = new DefaultTableModel();

    public Fournisseur(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();

        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }

        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));

        //Desactiver les bouton
        modifier.setEnabled(false);
        supprimer.setEnabled(false);
        vider.setEnabled(false);

        //Ajouter les colonnes
        model.addColumn("Nom du fournisseur");
        model.addColumn("Adresse");
        model.addColumn("Telephone");
        model.addColumn("Email");
        model.addColumn("Site");
        resultat = stmt.executeQuery("Select * from fournisseur");
        Actualiser(resultat);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Formulaire = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        nom = new javax.swing.JTextField();
        adresse = new javax.swing.JTextField();
        tel = new javax.swing.JTextField();
        site = new javax.swing.JTextField();
        mail = new javax.swing.JTextField();
        ajouter = new javax.swing.JButton();
        vider = new javax.swing.JButton();
        modifier = new javax.swing.JButton();
        supprimer = new javax.swing.JButton();
        fermer = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Fournisseur -Administrateurs");

        Formulaire.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder()));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel1.setText("Nom");

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setText("Adresse ");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setText("Téléphone");

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setText("Email");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setText("Site web");

        nom.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        adresse.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        tel.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        site.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        mail.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        ajouter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ajouter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Ajouter.png"))); // NOI18N
        ajouter.setText("Ajouter");
        ajouter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ajouter.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        ajouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterActionPerformed(evt);
            }
        });

        vider.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        vider.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Vider.png"))); // NOI18N
        vider.setText("Vider les champs");
        vider.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        vider.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viderActionPerformed(evt);
            }
        });

        modifier.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });

        supprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        supprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Supprimer.png"))); // NOI18N
        supprimer.setText("Supprimer");
        supprimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        supprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supprimerActionPerformed(evt);
            }
        });

        fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        fermer.setText("Fermer");
        fermer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fermerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout FormulaireLayout = new javax.swing.GroupLayout(Formulaire);
        Formulaire.setLayout(FormulaireLayout);
        FormulaireLayout.setHorizontalGroup(
            FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormulaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addGroup(FormulaireLayout.createSequentialGroup()
                        .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(site, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                            .addComponent(mail)
                            .addComponent(adresse)
                            .addComponent(nom)
                            .addComponent(tel, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fermer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(supprimer, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(modifier, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ajouter, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(vider, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        FormulaireLayout.setVerticalGroup(
            FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(FormulaireLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ajouter))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(modifier))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(supprimer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(vider))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(FormulaireLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(site, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fermer))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Formulaire, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Formulaire, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public void Deplacer(int i) {
        //Deplacer le contenu des lignes dans les champs de text
        try {
            nom.setText(model.getValueAt(i, 0).toString());
            adresse.setText(model.getValueAt(i, 1).toString());
            tel.setText(model.getValueAt(i, 2).toString());
            mail.setText(model.getValueAt(i, 3).toString());
            site.setText(model.getValueAt(i, 4).toString());
        } catch (Exception e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void Actualiser(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    resultat.getString("nom_Fournisseur"),
                    resultat.getString("adresse_Fournisseur"),
                    resultat.getString("tel_Fournisseur"),
                    resultat.getString("email_Fournisseur"),
                    resultat.getString("site_Fournisseur"),
                    resultat.getString("id_Fournisseur"),});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tab.setModel(model);
    }

    private void ViderLeChamp() {
        //Vider le champ de test        
        nom.setText("");
        adresse.setText("");
        tel.setText("");
        mail.setText("");
        site.setText("");
    }

    private void ajouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajouterActionPerformed
        String requete = "insert into fournisseur("
                + "nom_Fournisseur,adresse_Fournisseur,tel_Fournisseur,email_Fournisseur,site_Fournisseur)VALUES('"
                + nom.getText() + "','"
                + adresse.getText() + "','"
                + tel.getText() + "','"
                + mail.getText() + "','"
                + site.getText() + "')";
        if (nom.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Le nom du fournisseur est obligatoire");
        } else if (adresse.getText().equals("") || tel.getText().equals("") || mail.getText().equals("") || site.getText().equals("")) {
            if (JOptionPane.showConfirmDialog(
                    null, "Il y a un ou plusieurs champs vide\nVous voulez quand même ajouter?",
                    "Confirmation d'ajout",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                try {
                    stmt.executeUpdate(requete);
                    resultat = stmt.executeQuery("Select * from fournisseur");
                    Actualiser(resultat);
                    ViderLeChamp();
                } catch (Exception ex) {
                    System.err.println(ex);
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
        }
    }//GEN-LAST:event_ajouterActionPerformed

    private void viderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viderActionPerformed
        ViderLeChamp();
        //Desactiver les bouton
        modifier.setEnabled(false);
        supprimer.setEnabled(false);
        vider.setEnabled(false);
    }//GEN-LAST:event_viderActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();

        String update = "update  fournisseur set "
                + "nom_Fournisseur = '" + nom.getText() + "',"
                + "adresse_Fournisseur = '" + adresse.getText() + "',"
                + "tel_Fournisseur = '" + tel.getText() + "',"
                + "email_Fournisseur = '" + mail.getText() + "',"
                + "site_Fournisseur = '" + site.getText() + "' where nom_Fournisseur = '" + selectionner + "'";

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez modifier cette fournisseur ?", "Confirmation de modification",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                stmt.executeUpdate(update);
                resultat = stmt.executeQuery("Select * from fournisseur");
                Actualiser(resultat);
                ViderLeChamp();
            }
        } catch (Exception ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }

    }//GEN-LAST:event_modifierActionPerformed

    private void supprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supprimerActionPerformed
        String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();

        try {
            if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment supprimer cette Fournisseur ?", "Confirmation de suppression",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                model.removeRow(tab.getSelectedRow());
                stmt.executeUpdate("DELETE FROM fournisseur WHERE  nom_Fournisseur = '" + selectionner + "'");
                resultat = stmt.executeQuery("Select * from fournisseur");
                Actualiser(resultat);
            }
        } catch (HeadlessException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
        ViderLeChamp();

    }//GEN-LAST:event_supprimerActionPerformed

    private void fermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fermerActionPerformed
        dispose();
    }//GEN-LAST:event_fermerActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        int i = tab.getSelectedRow();
        Deplacer(i);
        modifier.setEnabled(true);
        supprimer.setEnabled(true);
        vider.setEnabled(true);
    }//GEN-LAST:event_tabMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fournisseur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fournisseur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fournisseur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fournisseur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Fournisseur dialog = new Fournisseur(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Fournisseur.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Formulaire;
    private javax.swing.JTextField adresse;
    private javax.swing.JButton ajouter;
    private javax.swing.JButton fermer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField mail;
    private javax.swing.JButton modifier;
    private javax.swing.JTextField nom;
    private javax.swing.JTextField site;
    private javax.swing.JButton supprimer;
    private javax.swing.JTable tab;
    private javax.swing.JTextField tel;
    private javax.swing.JButton vider;
    // End of variables declaration//GEN-END:variables

}
