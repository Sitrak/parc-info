
package InterfaceAdmin;

import ConnectionBDD.Connecter;
import static InterfaceAdmin.MaterielFormulaire.ValiderModifier;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public final class Materiel extends javax.swing.JDialog  {

    Connecter conn = new Connecter();
    
    ResultSet resultat,rs;
    Statement stmt;
    DefaultTableModel model = new DefaultTableModel();
    
    public Materiel(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();
        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));
        
        Modifier.setEnabled(false);
        Supprimer.setEnabled(false);

        model.addColumn("Id");
        model.addColumn("Nom Materiel");
        model.addColumn("Marque");
        model.addColumn("Fournisseur");
        model.addColumn("Date achat");
        model.addColumn("Duree garantie");
        model.addColumn("Prix achat");
        model.addColumn("Situation");
        model.addColumn("Societe");
        model.addColumn("Description");
        
        resultat = stmt.executeQuery("Select * from materiel");
        Actualiser(resultat);
    }
    

       public void Actualiser(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    resultat.getString("id_Materiel"),
                    resultat.getString("nom_Mat"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("nom_Fournisseur"),
                    resultat.getString("date_Achat_Mat"),
                    resultat.getString("duree_Garantie_Mat"),
                    resultat.getString("prix_Mat"),
                    resultat.getString("mat_En_Stock"),
                    resultat.getString("nom_St"),
                    resultat.getString("description_Mat")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tab.setModel(model);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        Nouveau1 = new javax.swing.JButton();
        exporter = new javax.swing.JButton();
        Ajouter = new javax.swing.JButton();
        Modifier = new javax.swing.JButton();
        Supprimer = new javax.swing.JButton();
        Fermer = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel7 = new javax.swing.JPanel();
        Station1 = new javax.swing.JRadioButton();
        MachineEnStock1 = new javax.swing.JRadioButton();
        ToutMachine1 = new javax.swing.JRadioButton();
        txtRecherche = new javax.swing.JTextField();
        Chercher = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Liste des materiels - Administrateur");

        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        Nouveau1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Nouveau1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Imprimer.png"))); // NOI18N
        Nouveau1.setText("Imprimer");
        Nouveau1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Nouveau1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nouveau1ActionPerformed(evt);
            }
        });

        exporter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        exporter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/export.png"))); // NOI18N
        exporter.setText("Exporter");
        exporter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        exporter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exporterActionPerformed(evt);
            }
        });

        Ajouter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Ajouter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Nouveau.png"))); // NOI18N
        Ajouter.setText("Nouveau");
        Ajouter.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Ajouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AjouterActionPerformed(evt);
            }
        });

        Modifier.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Modifier.png"))); // NOI18N
        Modifier.setText("Modifier");
        Modifier.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModifierActionPerformed(evt);
            }
        });

        Supprimer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Supprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Supprimer.png"))); // NOI18N
        Supprimer.setText("Supprimer");
        Supprimer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Supprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SupprimerActionPerformed(evt);
            }
        });

        Fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        Fermer.setText("Fermer");
        Fermer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        Fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FermerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Supprimer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Ajouter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Nouveau1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Modifier, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(exporter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Fermer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1)
                    .addComponent(jSeparator2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Ajouter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Modifier)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Supprimer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(Nouveau1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exporter)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Fermer)
                .addContainerGap())
        );

        buttonGroup1.add(Station1);
        Station1.setText("En Stock");
        Station1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Station1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(MachineEnStock1);
        MachineEnStock1.setText("En Service");
        MachineEnStock1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MachineEnStock1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(ToutMachine1);
        ToutMachine1.setSelected(true);
        ToutMachine1.setText("Tous");
        ToutMachine1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ToutMachine1ActionPerformed(evt);
            }
        });

        txtRecherche.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtRecherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRechercheKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRechercheKeyTyped(evt);
            }
        });

        Chercher.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Chercher.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/recherher.png"))); // NOI18N
        Chercher.setText("Rechercher");
        Chercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChercherActionPerformed(evt);
            }
        });
        Chercher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ChercherKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ToutMachine1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
                .addComponent(Station1)
                .addGap(96, 96, 96)
                .addComponent(MachineEnStock1)
                .addGap(93, 93, 93)
                .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(Chercher))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(Chercher)
                .addComponent(MachineEnStock1)
                .addComponent(Station1)
                .addComponent(ToutMachine1))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtRechercheKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyPressed

    }//GEN-LAST:event_txtRechercheKeyPressed

    private void txtRechercheKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyTyped

    }//GEN-LAST:event_txtRechercheKeyTyped

    private void ChercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChercherActionPerformed
       try {
            resultat = stmt.executeQuery("Select * from materiel where "
                + "nom_Mat = '" + txtRecherche.getText() + "'"
                + "or nom_Marque = '" + txtRecherche.getText() + "'"
                + "or nom_Fournisseur = '" + txtRecherche.getText() + "'"
                + "or date_Achat_Mat = '" + txtRecherche.getText() + "'"
                + "or duree_Garantie_Mat = '" + txtRecherche.getText() + "'"
                + "or mat_En_Stock = '" + txtRecherche.getText() + "'"
                + "or prix_Mat= '" + txtRecherche.getText() + "'"
                + "or nom_St = '" + txtRecherche.getText() + "'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ChercherActionPerformed

    private void ChercherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ChercherKeyPressed

    }//GEN-LAST:event_ChercherKeyPressed

    private void SupprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SupprimerActionPerformed

        if (tab.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(null, "Veuillez selectionnez une materiel");
        } else {
            String selectionner = model.getValueAt(tab.getSelectedRow(), 0).toString();
            try {
                if (JOptionPane.showConfirmDialog(null, "Vous voulez vraiment supprimer cette materiel ?", "Confirmation Suppression",
                    JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION) {
                model.removeRow(tab.getSelectedRow());
                stmt.executeUpdate("DELETE FROM materiel WHERE  id_Materiel = '" + selectionner + "'");
                resultat = stmt.executeQuery("Select * from materiel");
                Actualiser(resultat);
                Modifier.setEnabled(false);
                Supprimer.setEnabled(false);
            }
        } catch (HeadlessException | SQLException e) {
            System.err.println(e);
            JOptionPane.showMessageDialog(null, e);
        }
        }
    }//GEN-LAST:event_SupprimerActionPerformed

    private void Nouveau1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nouveau1ActionPerformed
        MessageFormat header = new MessageFormat("Liste des materiels");
        MessageFormat footer = new MessageFormat("");
        try {
            tab.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } catch (PrinterException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Nouveau1ActionPerformed

    private void exporterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exporterActionPerformed
        ExporterExcel.exporter(this, tab);
    }//GEN-LAST:event_exporterActionPerformed

    private void AjouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AjouterActionPerformed
        MaterielFormulaire m = new MaterielFormulaire(new javax.swing.JFrame(), true);
        ValiderModifier.setEnabled(false);
        m.setVisible(true);
                
        if (m.isShowing() == false) {
            try {
                rs = stmt.executeQuery("Select * from materiel");
                Actualiser(rs);
                Modifier.setEnabled(false);
                Supprimer.setEnabled(false);
            } catch (SQLException ex) {
                Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_AjouterActionPerformed

    private void ModifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModifierActionPerformed
        MaterielFormulaire m = new MaterielFormulaire(new javax.swing.JFrame(), true);
        m.AfficheContenu();
        m.setVisible(true);
                if (m.isShowing() == false) {
            try {
                rs = stmt.executeQuery("Select * from materiel");
                Actualiser(rs);
                Modifier.setEnabled(false);
                Supprimer.setEnabled(false);
            } catch (SQLException ex) {
                Logger.getLogger(Logiciel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }//GEN-LAST:event_ModifierActionPerformed

    private void FermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FermerActionPerformed
        dispose();
    }//GEN-LAST:event_FermerActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        Modifier.setEnabled(true);
        Supprimer.setEnabled(true);
    }//GEN-LAST:event_tabMouseClicked

    private void Station1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Station1ActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from materiel where mat_En_Stock = 'En Stock'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Materiel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Station1ActionPerformed

    private void ToutMachine1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ToutMachine1ActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from materiel");
        } catch (SQLException ex) {
            Logger.getLogger(Materiel.class.getName()).log(Level.SEVERE, null, ex);
        }
        Actualiser(resultat);
    }//GEN-LAST:event_ToutMachine1ActionPerformed

    private void MachineEnStock1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MachineEnStock1ActionPerformed

        try {
            resultat = stmt.executeQuery("Select * from materiel where mat_En_Stock = 'En Service'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(Materiel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MachineEnStock1ActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Materiel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Materiel dialog = new Materiel(new javax.swing.JFrame(), true);
                    
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(Materiel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Ajouter;
    private javax.swing.JButton Chercher;
    private javax.swing.JButton Fermer;
    private javax.swing.JRadioButton MachineEnStock1;
    private javax.swing.JButton Modifier;
    private javax.swing.JButton Nouveau1;
    private javax.swing.JRadioButton Station1;
    private javax.swing.JButton Supprimer;
    private javax.swing.JRadioButton ToutMachine1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton exporter;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public static javax.swing.JTable tab;
    private javax.swing.JTextField txtRecherche;
    // End of variables declaration//GEN-END:variables
}
