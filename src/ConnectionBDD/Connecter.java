package ConnectionBDD;

import java.sql.*;

public class Connecter {

    Connection con;

    public Connecter() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch (ClassNotFoundException e){System.err.println(e);}
        try{
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/parc", "root", "123456");
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    public Connection obtenirConnexion() {
        return con;
    }
}
