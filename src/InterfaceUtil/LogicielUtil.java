package InterfaceUtil;

import InterfaceAdmin.*;
import ConnectionBDD.Connecter;
import java.awt.Toolkit;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class LogicielUtil extends javax.swing.JDialog {

    Connecter conn = new Connecter();
    
    ResultSet resultat;
    Statement stmt;
    DefaultTableModel model = new DefaultTableModel();

    public LogicielUtil(java.awt.Frame parent, boolean modal) throws SQLException {
        super(parent, modal);
        initComponents();

        try {
            stmt = conn.obtenirConnexion().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
        
        //Changer l'icon du fenetre
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("Icon.png")));
        

        //Ajout des colonnes liste
        model.addColumn("Id");
        model.addColumn("Type");
        model.addColumn("Nom du Logiciel");
        model.addColumn("Nom marque");
        model.addColumn("Date achat");
        model.addColumn("N° de serie");
        model.addColumn("Licence");
        model.addColumn("Prix d'achat");
        model.addColumn("Societe");
        model.addColumn("Description");

        //Afficher tous le machine au demarrage
        resultat = stmt.executeQuery("Select * from logiciel");
        Actualiser(resultat);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtGp = new javax.swing.ButtonGroup();
        Type = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        Tableau = new javax.swing.JPanel();
        Liste = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        Recherche = new javax.swing.JPanel();
        txtRecherche = new javax.swing.JTextField();
        Chercher = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        Fermer = new javax.swing.JButton();
        exporter = new javax.swing.JButton();
        Nouveau1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        Station = new javax.swing.JRadioButton();
        MachineEnStock = new javax.swing.JRadioButton();
        ToutMachine = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Listes des logciels - Utilisateurs");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });
        getContentPane().setLayout(new java.awt.CardLayout());

        Liste.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Liste des Logiciels", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Segoe UI", 0, 14))); // NOI18N

        tab.setAutoCreateRowSorter(true);
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tab);

        javax.swing.GroupLayout ListeLayout = new javax.swing.GroupLayout(Liste);
        Liste.setLayout(ListeLayout);
        ListeLayout.setHorizontalGroup(
            ListeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4)
        );
        ListeLayout.setVerticalGroup(
            ListeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ListeLayout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        txtRecherche.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtRecherche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRechercheActionPerformed(evt);
            }
        });
        txtRecherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRechercheKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRechercheKeyTyped(evt);
            }
        });

        Chercher.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Chercher.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/recherher.png"))); // NOI18N
        Chercher.setText("Rechercher");
        Chercher.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ChercherActionPerformed(evt);
            }
        });
        Chercher.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ChercherKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout RechercheLayout = new javax.swing.GroupLayout(Recherche);
        Recherche.setLayout(RechercheLayout);
        RechercheLayout.setHorizontalGroup(
            RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RechercheLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Chercher)
                .addContainerGap())
        );
        RechercheLayout.setVerticalGroup(
            RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RechercheLayout.createSequentialGroup()
                .addGroup(RechercheLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRecherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Chercher))
                .addGap(0, 2, Short.MAX_VALUE))
        );

        Fermer.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Fermer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/cancel.png"))); // NOI18N
        Fermer.setText("Fermer");
        Fermer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FermerActionPerformed(evt);
            }
        });

        exporter.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        exporter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/export.png"))); // NOI18N
        exporter.setText("Exporter");
        exporter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exporterActionPerformed(evt);
            }
        });

        Nouveau1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        Nouveau1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/InterfaceAdmin/icon/Imprimer.png"))); // NOI18N
        Nouveau1.setText("Imprimer");
        Nouveau1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Nouveau1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(Nouveau1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(exporter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(851, 851, 851)
                .addComponent(Fermer, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Fermer, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exporter)
                    .addComponent(Nouveau1))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        buttonGroup1.add(Station);
        Station.setText("Gratuit");
        Station.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StationActionPerformed(evt);
            }
        });

        buttonGroup1.add(MachineEnStock);
        MachineEnStock.setText("Payant");
        MachineEnStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MachineEnStockActionPerformed(evt);
            }
        });

        buttonGroup1.add(ToutMachine);
        ToutMachine.setSelected(true);
        ToutMachine.setText("Tous");
        ToutMachine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ToutMachineActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addComponent(ToutMachine)
                .addGap(180, 180, 180)
                .addComponent(Station)
                .addGap(223, 223, 223)
                .addComponent(MachineEnStock)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(MachineEnStock)
                .addComponent(ToutMachine)
                .addComponent(Station))
        );

        javax.swing.GroupLayout TableauLayout = new javax.swing.GroupLayout(Tableau);
        Tableau.setLayout(TableauLayout);
        TableauLayout.setHorizontalGroup(
            TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableauLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Liste, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, TableauLayout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(Recherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        TableauLayout.setVerticalGroup(
            TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TableauLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(TableauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Recherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Liste, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(Tableau, "card2");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public void Actualiser(ResultSet resultat) {

        //Ajouter les contenus dans le tableau
        try {
            model.setRowCount(0);

            while (resultat.next()) {
                model.addRow(new Object[]{
                    resultat.getString("id_Log"),
                    resultat.getString("type_Log"),
                    resultat.getString("nom_log"),
                    resultat.getString("nom_Marque"),
                    resultat.getString("date_Achat"),
                    resultat.getString("serial_log"),
                    resultat.getString("max_licence"),
                    resultat.getString("prix_Achat"),
                    resultat.getString("nom_St"),
                    resultat.getString("description_Log")});
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        tab.setModel(model);
    }

    private void FermerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FermerActionPerformed
        dispose();
    }//GEN-LAST:event_FermerActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
 
    }//GEN-LAST:event_tabMouseClicked

    private void Nouveau1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Nouveau1ActionPerformed
        MessageFormat header = new MessageFormat("Liste des logiciels");
        MessageFormat footer = new MessageFormat("");
        try {
            tab.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        } catch (PrinterException ex) {
            Logger.getLogger(ListeMachine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_Nouveau1ActionPerformed

    private void ChercherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ChercherActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from logiciel where "
                    + "id_Log = '" + txtRecherche.getText() + "'"
                    + "or type_Log = '" + txtRecherche.getText() + "'"
                    + "or nom_log = '" + txtRecherche.getText() + "'"
                    + "or nom_Marque = '" + txtRecherche.getText() + "'"
                    + "or date_Achat = '" + txtRecherche.getText() + "'"
                    + "or serial_log = '" + txtRecherche.getText() + "'"
                    + "or max_licence = '" + txtRecherche.getText() + "'"
                    + "or prix_Achat = '" + txtRecherche.getText() + "'"
                    + "or nom_St = '" + txtRecherche.getText() + "'"
                    + "or description_Log = '" + txtRecherche.getText() + "'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(LogicielUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_ChercherActionPerformed

    private void txtRechercheKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyPressed

    }//GEN-LAST:event_txtRechercheKeyPressed

    private void txtRechercheKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRechercheKeyTyped


    }//GEN-LAST:event_txtRechercheKeyTyped

    private void ChercherKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ChercherKeyPressed

    }//GEN-LAST:event_ChercherKeyPressed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        //remplirComboBox_Util();
    }//GEN-LAST:event_formWindowClosed

    private void exporterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exporterActionPerformed
        ExporterExcel.exporter(this, tab);
    }//GEN-LAST:event_exporterActionPerformed

    private void StationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StationActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from logiciel where type_Log = 'Gratuit'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(LogicielUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_StationActionPerformed

    private void MachineEnStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MachineEnStockActionPerformed

        try {
            resultat = stmt.executeQuery("Select * from logiciel where type_Log = 'Payant'");
            Actualiser(resultat);
        } catch (SQLException ex) {
            Logger.getLogger(LogicielUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_MachineEnStockActionPerformed

    private void ToutMachineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ToutMachineActionPerformed
        try {
            resultat = stmt.executeQuery("Select * from logiciel");
        } catch (SQLException ex) {
            Logger.getLogger(LogicielUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        Actualiser(resultat);
    }//GEN-LAST:event_ToutMachineActionPerformed

    private void txtRechercheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRechercheActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtRechercheActionPerformed

    public static void main(String args[]) throws SQLException {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LogicielUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LogicielUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LogicielUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LogicielUtil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }


        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    LogicielUtil dialog = new LogicielUtil(new javax.swing.JFrame(), true);

                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(LogicielUtil.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup BtGp;
    private javax.swing.JButton Chercher;
    private javax.swing.JButton Fermer;
    private javax.swing.JPanel Liste;
    private javax.swing.JRadioButton MachineEnStock;
    private javax.swing.JButton Nouveau1;
    private javax.swing.JPanel Recherche;
    private javax.swing.JRadioButton Station;
    private javax.swing.JPanel Tableau;
    private javax.swing.JRadioButton ToutMachine;
    private javax.swing.ButtonGroup Type;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton exporter;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane4;
    private static javax.swing.JTable tab;
    private javax.swing.JTextField txtRecherche;
    // End of variables declaration//GEN-END:variables
}
